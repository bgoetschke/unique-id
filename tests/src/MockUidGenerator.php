<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\UniqueID;

use BjoernGoetschke\UniqueID\UidGeneratorInterface;
use Closure;
use PHPUnit\Framework\TestCase;

final class MockUidGenerator implements UidGeneratorInterface
{
    private ?Closure $callback;

    public function __construct(?Closure $callback = null)
    {
        $this->callback = $callback;
    }

    public function generate(int $minLength = 0, int $maxLength = 0): string
    {
        $callback = $this->callback;
        if (!($callback instanceof Closure)) {
            TestCase::fail('MockGenerator was not expected to be called!');
        }
        return $callback($minLength, $maxLength);
    }
}
