<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\UniqueID\Unit;

use BjoernGoetschke\UniqueID\UlidUidGenerator;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use RuntimeException;

final class UlidGeneratorTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        resetMocks();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        resetMocks();
    }

    public function testClone(): void
    {
        $generator1 = new UlidUidGenerator();
        $generator2 = clone $generator1;

        self::assertNotSame($generator1, $generator2);
    }

    public function testSerialize(): void
    {
        $generator1 = new UlidUidGenerator();
        $generator2 = unserialize(serialize($generator1));

        self::assertInstanceOf(
            UlidUidGenerator::class,
            $generator2,
        );

        self::assertNotSame($generator1, $generator2);
    }

    public function testGenerateRequestedTooShortUlidException(): void
    {
        $generator = new UlidUidGenerator();

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage(
            'Length specification does not allow an ' .
            'universally unique lexicographically sortable identifier.',
        );

        $generator->generate(0, 25);
    }

    public function testGenerateRequestedTooLongUlidException(): void
    {
        $generator = new UlidUidGenerator();

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage(
            'Length specification does not allow an ' .
            'universally unique lexicographically sortable identifier.',
        );

        $generator->generate(27, 0);
    }

    public function testGenerateCorrectUlidFormat(): void
    {
        $generator = new UlidUidGenerator();
        $uid = $generator->generate();

        self::assertMatchesRegularExpression(
            '=^[0-9A-Z]{26}$=',
            $uid,
        );
    }

    public function testGenerateTimestampAtMaximumReturnsCorrectUlid(): void
    {
        global $mock_microtime;
        $mock_microtime_output = [
            281474976710.655,
        ];
        $mock_microtime = function ($get_as_float = false) use (&$mock_microtime_output) {
            self::assertTrue($get_as_float);
            self::assertNotCount(0, $mock_microtime_output);
            return array_shift($mock_microtime_output);
        };

        $generator = new UlidUidGenerator();

        self::assertMatchesRegularExpression(
            '=^7[Z]{9}[0-9A-Z]{16}$=',
            $generator->generate(),
        );

        self::assertCount(
            0,
            $mock_microtime_output,
        );
    }

    public function testGenerateTimestampJustAboveMaximumThrowsException(): void
    {
        global $mock_microtime;
        $mock_microtime_output = [
            281474976710.656,
        ];
        $mock_microtime = function ($get_as_float = false) use (&$mock_microtime_output) {
            self::assertTrue($get_as_float);
            self::assertNotCount(0, $mock_microtime_output);
            return array_shift($mock_microtime_output);
        };

        $generator = new UlidUidGenerator();

        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage(
            'Unable to generate an universally unique lexicographically sortable identifier, ' .
            'timestamp is not exactly 48 bits.',
        );

        $generator->generate();
    }

    public function testGenerateFixedTimestamp20000101(): void
    {
        global $mock_microtime;
        $mock_microtime_output = [
            946684800,
        ];
        $mock_microtime = function ($get_as_float = false) use (&$mock_microtime_output) {
            self::assertTrue($get_as_float);
            self::assertNotCount(0, $mock_microtime_output);
            return array_shift($mock_microtime_output);
        };

        $generator = new UlidUidGenerator();

        self::assertMatchesRegularExpression(
            '=^00VHNCZB00[0-9A-Z]{16}$=',
            $generator->generate(),
        );

        self::assertCount(
            0,
            $mock_microtime_output,
        );
    }

    public function testMicrotimeGetsProperlyTruncated(): void
    {
        global $mock_microtime;
        $mock_microtime_output = [
            946684800.000123456,
        ];
        $mock_microtime = function ($get_as_float = false) use (&$mock_microtime_output) {
            self::assertTrue($get_as_float);
            self::assertNotCount(0, $mock_microtime_output);
            return array_shift($mock_microtime_output);
        };

        $generator = new UlidUidGenerator();

        self::assertMatchesRegularExpression(
            '=^00VHNCZB00[0-9A-Z]{16}$=',
            $generator->generate(),
        );

        self::assertCount(
            0,
            $mock_microtime_output,
        );
    }

    public function testGenerateFixedTimestamp30000101(): void
    {
        global $mock_microtime;
        $mock_microtime_output = [
            32503680000,
        ];
        $mock_microtime = function ($get_as_float = false) use (&$mock_microtime_output) {
            self::assertTrue($get_as_float);
            self::assertNotCount(0, $mock_microtime_output);
            return array_shift($mock_microtime_output);
        };

        $generator = new UlidUidGenerator();

        self::assertMatchesRegularExpression(
            '=^0XHZD4SR00[0-9A-Z]{16}$=',
            $generator->generate(),
        );

        self::assertCount(
            0,
            $mock_microtime_output,
        );
    }

    public function testGenerateFixedTimestamp60000101(): void
    {
        global $mock_microtime;
        $mock_microtime_output = [
            127174492800,
        ];
        $mock_microtime = function ($get_as_float = false) use (&$mock_microtime_output) {
            self::assertTrue($get_as_float);
            self::assertNotCount(0, $mock_microtime_output);
            return array_shift($mock_microtime_output);
        };

        $generator = new UlidUidGenerator();

        self::assertMatchesRegularExpression(
            '=^3KN8F7FH00[0-9A-Z]{16}$=',
            $generator->generate(),
        );

        self::assertCount(
            0,
            $mock_microtime_output,
        );
    }

    public function testGenerateFixedTimestamp99990101(): void
    {
        global $mock_microtime;
        $mock_microtime_output = [
            253370764800,
        ];
        $mock_microtime = function ($get_as_float = false) use (&$mock_microtime_output) {
            self::assertTrue($get_as_float);
            self::assertNotCount(0, $mock_microtime_output);
            return array_shift($mock_microtime_output);
        };

        $generator = new UlidUidGenerator();

        self::assertMatchesRegularExpression(
            '=^76E1X6XC00[0-9A-Z]{16}$=',
            $generator->generate(),
        );

        self::assertCount(
            0,
            $mock_microtime_output,
        );
    }

    public function testGenerateFixedTimestampEndOf99991231(): void
    {
        global $mock_microtime;
        $mock_microtime_output = [
            253402300799.9999999999,
        ];
        $mock_microtime = function ($get_as_float = false) use (&$mock_microtime_output) {
            self::assertTrue($get_as_float);
            self::assertNotCount(0, $mock_microtime_output);
            return array_shift($mock_microtime_output);
        };

        $generator = new UlidUidGenerator();

        self::assertMatchesRegularExpression(
            '=^76EZ91ZQ00[0-9A-Z]{16}$=',
            $generator->generate(),
        );

        self::assertCount(
            0,
            $mock_microtime_output,
        );
    }
}
