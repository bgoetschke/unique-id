<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\UniqueID\Unit;

use BjoernGoetschke\Test\UniqueID\MockUidGenerator;
use BjoernGoetschke\UniqueID\AddAffixUidGeneratorDecorator;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class AddAffixGeneratorTest extends TestCase
{
    public function testGetSetPrefix(): void
    {
        $generator = new AddAffixUidGeneratorDecorator(new MockUidGenerator());

        self::assertSame(
            '',
            $generator->getPrefix(),
        );

        $generator->setPrefix('some_prefix');

        self::assertSame(
            'some_prefix',
            $generator->getPrefix(),
        );
    }

    public function testSetPrefixInvalidArgumentException(): void
    {
        $generator = new AddAffixUidGeneratorDecorator(new MockUidGenerator());

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid prefix specified: !12345');

        $generator->setPrefix('!12345');
    }

    public function testGetSetSuffix(): void
    {
        $generator = new AddAffixUidGeneratorDecorator(new MockUidGenerator());

        self::assertSame(
            '',
            $generator->getSuffix(),
        );

        $generator->setSuffix('some_suffix');

        self::assertSame(
            'some_suffix',
            $generator->getSuffix(),
        );
    }

    public function testSetSuffixInvalidArgumentException(): void
    {
        $generator = new AddAffixUidGeneratorDecorator(new MockUidGenerator());

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid suffix specified: !12345');

        $generator->setSuffix('!12345');
    }

    public function testGetSetGenerator(): void
    {
        $generator1 = new MockUidGenerator();
        $generator2 = new MockUidGenerator();
        $generator3 = new AddAffixUidGeneratorDecorator($generator1);

        self::assertSame(
            $generator1,
            $generator3->getGenerator(),
        );

        $generator3->setGenerator($generator2);

        self::assertSame(
            $generator2,
            $generator3->getGenerator(),
        );
    }

    public function testClone(): void
    {
        $generator1 = new MockUidGenerator();
        $generator2 = new AddAffixUidGeneratorDecorator($generator1, 'some_prefix', 'some_suffix');
        $generator3 = clone $generator2;

        self::assertNotSame(
            $generator2,
            $generator3,
        );

        self::assertSame(
            $generator2->getGenerator(),
            $generator3->getGenerator(),
        );

        self::assertSame(
            'some_prefix',
            $generator3->getPrefix(),
        );

        self::assertSame(
            'some_suffix',
            $generator3->getSuffix(),
        );
    }

    public function testSerialize(): void
    {
        $generator1 = new MockUidGenerator();
        $generator2 = new AddAffixUidGeneratorDecorator($generator1, 'some_prefix', 'some_suffix');
        $generator3 = unserialize(serialize($generator2));

        self::assertInstanceOf(
            AddAffixUidGeneratorDecorator::class,
            $generator3,
        );

        self::assertNotSame(
            $generator2,
            $generator3,
        );

        self::assertNotSame(
            $generator2->getGenerator(),
            $generator3->getGenerator(),
        );

        self::assertSame(
            'some_prefix',
            $generator3->getPrefix(),
        );

        self::assertSame(
            'some_suffix',
            $generator3->getSuffix(),
        );
    }

    public function testGenerateAffixExceedsMaximumLengthException(): void
    {
        $generator1 = new MockUidGenerator();
        $generator2 = new AddAffixUidGeneratorDecorator($generator1, 'prefix', 'suffix');

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Affix length of 12 exceeds maximum allowed length of 10.');

        $generator2->generate(0, 10);
    }

    public function testGenerateReturnCorrectUid(): void
    {
        $generator1 = new MockUidGenerator(
            function ($min, $max) {
                self::assertSame(0, $min);
                self::assertSame(0, $max);
                return '1234';
            },
        );
        $generator2 = new AddAffixUidGeneratorDecorator($generator1, 'prefix', 'suffix');

        self::assertSame(
            'prefix1234suffix',
            $generator2->generate(),
        );
    }

    public function testGeneratePassCorrectMinAndMaxLength(): void
    {
        $generator1 = new MockUidGenerator(
            function ($min, $max) {
                self::assertSame(3, $min);
                self::assertSame(8, $max);
                return '1234';
            },
        );
        $generator2 = new AddAffixUidGeneratorDecorator($generator1, 'prefix', 'suffix');

        self::assertSame(
            'prefix1234suffix',
            $generator2->generate(15, 20),
        );
    }

    public function testGenerateExceptionOnTooShortUid(): void
    {
        $generator1 = new MockUidGenerator(
            function ($min, $max) {
                self::assertSame(3, $min);
                self::assertSame(8, $max);
                return '';
            },
        );
        $generator2 = new AddAffixUidGeneratorDecorator($generator1, 'prefix', 'suffix');

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Unable to generate an identifier with a minimum length of 15 characters.');

        $generator2->generate(15, 20);
    }

    public function testGenerateExceptionOnTooLongUid(): void
    {
        $generator1 = new MockUidGenerator(
            function ($min, $max) {
                self::assertSame(3, $min);
                self::assertSame(8, $max);
                return '1234567890';
            },
        );
        $generator2 = new AddAffixUidGeneratorDecorator($generator1, 'prefix', 'suffix');

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Unable to generate an identifier with a maximum length of 20 characters.');

        $generator2->generate(15, 20);
    }

    public function testZeroMaxLengthIsNotPassedAsNegativeValue(): void
    {
        $generator1 = new MockUidGenerator(
            function ($min, $max) {
                self::assertSame(0, $min);
                self::assertSame(0, $max);
                return '1234567890';
            },
        );
        $generator2 = new AddAffixUidGeneratorDecorator($generator1, 'prefix', 'suffix');

        self::assertSame(
            'prefix1234567890suffix',
            $generator2->generate(10, 0),
        );
    }
}
