<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\UniqueID\Unit;

use BjoernGoetschke\Test\UniqueID\MockUidGenerator;
use BjoernGoetschke\UniqueID\CheckCollisionUidGeneratorDecorator;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class CheckCollisionGeneratorTest extends TestCase
{
    public function testGetSetGenerator(): void
    {
        $callback1 = function () {
        };
        $generator1 = new MockUidGenerator();
        $generator2 = new MockUidGenerator();
        $generator3 = new CheckCollisionUidGeneratorDecorator($generator1, $callback1);

        self::assertSame(
            $generator1,
            $generator3->getGenerator(),
        );

        $generator3->setGenerator($generator2);

        self::assertSame(
            $generator2,
            $generator3->getGenerator(),
        );
    }

    public function testGetSetCallback(): void
    {
        $callback1 = function () {
        };
        $callback2 = function () {
        };
        $generator1 = new MockUidGenerator();
        $generator2 = new CheckCollisionUidGeneratorDecorator($generator1, $callback1);

        self::assertSame(
            $callback1,
            $generator2->getCallback(),
        );

        $generator2->setCallback($callback2);

        self::assertSame(
            $callback2,
            $generator2->getCallback(),
        );
    }

    public function testGetSetInitialCount(): void
    {
        $generator = new CheckCollisionUidGeneratorDecorator(
            new MockUidGenerator(),
            function () {
            },
        );

        self::assertSame(
            1,
            $generator->getInitialCount(),
        );

        $generator->setInitialCount(5);

        self::assertSame(
            5,
            $generator->getInitialCount(),
        );
    }

    public function testSetInitialCountExceptionBelowOne(): void
    {
        $generator = new CheckCollisionUidGeneratorDecorator(
            new MockUidGenerator(),
            function () {
            },
        );

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Number of initially generated unique identifiers must not be less than 1.');

        $generator->setInitialCount(0);
    }

    public function testGetSetCollisionMultiplier(): void
    {
        $generator = new CheckCollisionUidGeneratorDecorator(
            new MockUidGenerator(),
            function () {
            },
        );

        self::assertSame(
            2,
            $generator->getCollisionMultiplier(),
        );

        $generator->setCollisionMultiplier(7);

        self::assertSame(
            7,
            $generator->getCollisionMultiplier(),
        );
    }

    public function testSetCollisionMultiplierExceptionBelowZero(): void
    {
        $generator = new CheckCollisionUidGeneratorDecorator(
            new MockUidGenerator(),
            function () {
            },
        );

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Collision multiplier must not be less than 0.');

        $generator->setCollisionMultiplier(-1);
    }

    public function testGetSetMaximumTries(): void
    {
        $generator = new CheckCollisionUidGeneratorDecorator(
            new MockUidGenerator(),
            function () {
            },
        );

        self::assertSame(
            4,
            $generator->getMaximumTries(),
        );

        $generator->setMaximumTries(15);

        self::assertSame(
            15,
            $generator->getMaximumTries(),
        );
    }

    public function testSetMaximumTriesExceptionBelowOne(): void
    {
        $generator = new CheckCollisionUidGeneratorDecorator(
            new MockUidGenerator(),
            function () {
            },
        );

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Maximum number of tries must not be less than 1.');

        $generator->setMaximumTries(0);
    }

    public function testClone(): void
    {
        $callback1 = function () {
        };
        $generator1 = new MockUidGenerator();
        $generator2 = new CheckCollisionUidGeneratorDecorator($generator1, $callback1);
        $generator3 = clone $generator2;

        self::assertNotSame(
            $generator2,
            $generator3,
        );

        self::assertSame(
            $generator2->getGenerator(),
            $generator3->getGenerator(),
        );

        self::assertSame(
            $generator2->getCallback(),
            $generator3->getCallback(),
        );
    }

    public function testGenerateSuccess(): void
    {
        $callback1input = [
            ['uid1'],
            ['uid2', 'uid3'],
            ['uid4', 'uid5', 'uid6', 'uid7'],
            ['uid8', 'uid9', 'uid10', 'uid11', 'uid12', 'uid13', 'uid14', 'uid15'],
        ];
        $callback1output = [
            null,
            'someOtherString',
            (object)[],
            'uid12',
        ];
        $callback1 = function (array $uids) use (&$callback1input, &$callback1output) {
            self::assertSame(array_shift($callback1input), $uids);
            return array_shift($callback1output);
        };
        $generator1output = [
            'uid1',
            'uid2',
            'uid3',
            'uid4',
            'uid5',
            'uid6',
            'uid7',
            'uid8',
            'uid9',
            'uid10',
            'uid11',
            'uid12',
            'uid13',
            'uid14',
            'uid15',
        ];
        $generator1 = new MockUidGenerator(
            function ($min, $max) use (&$generator1output) {
                self::assertSame(10, $min);
                self::assertSame(20, $max);
                return array_shift($generator1output);
            },
        );
        $generator2 = new CheckCollisionUidGeneratorDecorator($generator1, $callback1);

        $uid = $generator2->generate(10, 20);

        self::assertSame(
            'uid12',
            $uid,
        );

        self::assertCount(
            0,
            $callback1input,
        );

        self::assertCount(
            0,
            $callback1output,
        );

        self::assertCount(
            0,
            $generator1output,
        );
    }

    public function testGenerateCollisionException(): void
    {
        $callback1input = [
            ['uid'],
            ['uid', 'uid'],
            ['uid', 'uid', 'uid', 'uid'],
            ['uid', 'uid', 'uid', 'uid', 'uid', 'uid', 'uid', 'uid'],
        ];
        $callback1output = [
            null,
            null,
            null,
            null,
        ];
        $callback1 = function (array $uids) use (&$callback1input, &$callback1output) {
            self::assertSame(array_shift($callback1input), $uids);
            return array_shift($callback1output);
        };
        $generator1output = [
            'uid',
            'uid',
            'uid',
            'uid',
            'uid',
            'uid',
            'uid',
            'uid',
            'uid',
            'uid',
            'uid',
            'uid',
            'uid',
            'uid',
            'uid',
        ];
        $generator1 = new MockUidGenerator(
            function ($min, $max) use (&$generator1output) {
                self::assertSame(10, $min);
                self::assertSame(20, $max);
                return array_shift($generator1output);
            },
        );
        $generator2 = new CheckCollisionUidGeneratorDecorator($generator1, $callback1);

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Failed to generate a unique identifier without a collision.');

        try {
            $generator2->generate(10, 20);
        } catch (InvalidArgumentException $e) {
            self::assertCount(
                0,
                $callback1input,
            );

            self::assertCount(
                0,
                $callback1output,
            );

            self::assertCount(
                0,
                $generator1output,
            );

            throw $e;
        }
    }

    public function testGenerateCallbackArgumentByReferenceNoEffect(): void
    {
        $callback1 = function (&$list) {
            $list = ['someRandomString'];
            return 'someRandomString';
        };
        $generator1output = [
            'uid',
            'uid',
            'uid',
            'uid',
            'uid',
            'uid',
            'uid',
            'uid',
            'uid',
            'uid',
            'uid',
            'uid',
            'uid',
            'uid',
            'uid',
        ];
        $generator1 = new MockUidGenerator(
            function ($min, $max) use (&$generator1output) {
                self::assertSame(10, $min);
                self::assertSame(20, $max);
                return array_shift($generator1output);
            },
        );
        $generator2 = new CheckCollisionUidGeneratorDecorator($generator1, $callback1);

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Failed to generate a unique identifier without a collision.');

        // suppress the warning raised by php because the callback defined
        // the first argument to be passed by reference
        $reporting = error_reporting(error_reporting() & ~E_WARNING);

        try {
            $generator2->generate(10, 20);
        } finally {
            error_reporting($reporting);
        }
    }
}
