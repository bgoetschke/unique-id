<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\UniqueID\Unit;

use BjoernGoetschke\UniqueID\Uuid4UidGenerator;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class Uuid4GeneratorTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        resetMocks();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        resetMocks();
    }

    public function testClone(): void
    {
        $generator1 = new Uuid4UidGenerator();
        $generator2 = clone $generator1;

        self::assertNotSame($generator1, $generator2);
    }

    public function testSerialize(): void
    {
        $generator1 = new Uuid4UidGenerator();
        $generator2 = unserialize(serialize($generator1));

        self::assertInstanceOf(
            Uuid4UidGenerator::class,
            $generator2,
        );

        self::assertNotSame($generator1, $generator2);
    }

    public function testGenerateRequestedTooShortUidException(): void
    {
        $generator = new Uuid4UidGenerator();

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Length specification does not allow an universally unique identifier.');

        $generator->generate(0, 35);
    }

    public function testGenerateRequestedTooLongUidException(): void
    {
        $generator = new Uuid4UidGenerator();

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Length specification does not allow an universally unique identifier.');

        $generator->generate(37, 0);
    }

    public function testGenerateCorrectUidFormat(): void
    {
        $generator = new Uuid4UidGenerator();
        $uid = $generator->generate();

        self::assertMatchesRegularExpression(
            '=^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$=',
            $uid,
        );
    }

    public function testGeneratorZerosGetExpandedToCorrectLength(): void
    {
        global $mock_function_exists;
        $mock_function_exists = function ($function_name) {
            if (in_array(mb_strtolower($function_name), ['random_bytes', 'openssl_random_pseudo_bytes'], true)) {
                return false;
            }
            return \function_exists($function_name);
        };

        global $mock_random_int;
        $mock_random_int = function ($min, $max) {
            self::assertSame(0, $min);
            self::assertSame(255, $max);
            return 0;
        };

        $generator = new Uuid4UidGenerator();
        $uid = $generator->generate();

        self::assertSame(
            '00000000-0000-4000-8000-000000000000',
            $uid,
        );
    }
}
