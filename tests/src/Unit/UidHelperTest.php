<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\UniqueID\Unit;

use BjoernGoetschke\UniqueID\UidHelper;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class UidHelperTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        resetMocks();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        resetMocks();
    }

    public function testPadStringToModLength(): void
    {
        self::assertSame(
            '__TestString',
            UidHelper::padStringToModLength('TestString', 6, '_', STR_PAD_LEFT),
        );

        self::assertSame(
            'TestString__',
            UidHelper::padStringToModLength('TestString', 6, '_', STR_PAD_RIGHT),
        );

        self::assertSame(
            '_TestString_',
            UidHelper::padStringToModLength('TestString', 6, '_', STR_PAD_BOTH),
        );
    }

    public function testDec2BinConversion(): void
    {
        self::assertSame(
            '0',
            UidHelper::dec2bin('0'),
        );

        self::assertSame(
            '101010',
            UidHelper::dec2bin('42'),
        );

        self::assertSame(
            '10100111001',
            UidHelper::dec2bin('1337'),
        );
    }

    public function testDec2BinConversionAround32BitWithoutGmp(): void
    {
        global $mock_extension_loaded;
        $mock_extension_loaded = function ($name) {
            if (mb_strtolower($name) === 'gmp') {
                return false;
            }
            return \extension_loaded($name);
        };

        self::assertSame(
            str_repeat('1', 29),
            UidHelper::dec2bin('536870911'),
        );

        self::assertSame(
            str_repeat('1', 30),
            UidHelper::dec2bin('1073741823'),
        );

        self::assertSame(
            str_repeat('1', 31),
            UidHelper::dec2bin('2147483647'),
        );

        self::assertSame(
            str_repeat('1', 32),
            UidHelper::dec2bin('4294967295'),
        );

        self::assertSame(
            str_repeat('1', 33),
            UidHelper::dec2bin('8589934591'),
        );

        self::assertSame(
            str_repeat('1', 34),
            UidHelper::dec2bin('17179869183'),
        );

        self::assertSame(
            str_repeat('1', 35),
            UidHelper::dec2bin('34359738367'),
        );
    }

    public function testDec2BinConversionAround32BitWithGmp(): void
    {
        if (!extension_loaded('gmp')) {
            self::markTestSkipped('GMP extension is required to run this test.');
        }

        self::assertSame(
            str_repeat('1', 29),
            UidHelper::dec2bin('536870911'),
        );

        self::assertSame(
            str_repeat('1', 30),
            UidHelper::dec2bin('1073741823'),
        );

        self::assertSame(
            str_repeat('1', 31),
            UidHelper::dec2bin('2147483647'),
        );

        self::assertSame(
            str_repeat('1', 32),
            UidHelper::dec2bin('4294967295'),
        );

        self::assertSame(
            str_repeat('1', 33),
            UidHelper::dec2bin('8589934591'),
        );

        self::assertSame(
            str_repeat('1', 34),
            UidHelper::dec2bin('17179869183'),
        );

        self::assertSame(
            str_repeat('1', 35),
            UidHelper::dec2bin('34359738367'),
        );
    }

    public function testDec2BinConversionAround64BitWithoutGmp(): void
    {
        global $mock_extension_loaded;
        $mock_extension_loaded = function ($name) {
            if (mb_strtolower($name) === 'gmp') {
                return false;
            }
            return \extension_loaded($name);
        };

        self::assertSame(
            str_repeat('1', 61),
            UidHelper::dec2bin('2305843009213693951'),
        );

        self::assertSame(
            str_repeat('1', 62),
            UidHelper::dec2bin('4611686018427387903'),
        );

        self::assertSame(
            str_repeat('1', 63),
            UidHelper::dec2bin('9223372036854775807'),
        );

        self::assertSame(
            str_repeat('1', 64),
            UidHelper::dec2bin('18446744073709551615'),
        );

        self::assertSame(
            str_repeat('1', 65),
            UidHelper::dec2bin('36893488147419103231'),
        );

        self::assertSame(
            str_repeat('1', 66),
            UidHelper::dec2bin('73786976294838206463'),
        );

        self::assertSame(
            str_repeat('1', 67),
            UidHelper::dec2bin('147573952589676412927'),
        );
    }

    public function testDec2BinConversionAround64BitWithGmp(): void
    {
        if (!extension_loaded('gmp')) {
            self::markTestSkipped('GMP extension is required to run this test.');
        }

        self::assertSame(
            str_repeat('1', 61),
            UidHelper::dec2bin('2305843009213693951'),
        );

        self::assertSame(
            str_repeat('1', 62),
            UidHelper::dec2bin('4611686018427387903'),
        );

        self::assertSame(
            str_repeat('1', 63),
            UidHelper::dec2bin('9223372036854775807'),
        );

        self::assertSame(
            str_repeat('1', 64),
            UidHelper::dec2bin('18446744073709551615'),
        );

        self::assertSame(
            str_repeat('1', 65),
            UidHelper::dec2bin('36893488147419103231'),
        );

        self::assertSame(
            str_repeat('1', 66),
            UidHelper::dec2bin('73786976294838206463'),
        );

        self::assertSame(
            str_repeat('1', 67),
            UidHelper::dec2bin('147573952589676412927'),
        );
    }

    public function testDec2BinConversionAround128BitWithoutGmp(): void
    {
        global $mock_extension_loaded;
        $mock_extension_loaded = function ($name) {
            if (mb_strtolower($name) === 'gmp') {
                return false;
            }
            return \extension_loaded($name);
        };

        self::assertSame(
            str_repeat('1', 125),
            UidHelper::dec2bin('42535295865117307932921825928971026431'),
        );

        self::assertSame(
            str_repeat('1', 126),
            UidHelper::dec2bin('85070591730234615865843651857942052863'),
        );

        self::assertSame(
            str_repeat('1', 127),
            UidHelper::dec2bin('170141183460469231731687303715884105727'),
        );

        self::assertSame(
            str_repeat('1', 128),
            UidHelper::dec2bin('340282366920938463463374607431768211455'),
        );

        self::assertSame(
            str_repeat('1', 129),
            UidHelper::dec2bin('680564733841876926926749214863536422911'),
        );

        self::assertSame(
            str_repeat('1', 130),
            UidHelper::dec2bin('1361129467683753853853498429727072845823'),
        );

        self::assertSame(
            str_repeat('1', 131),
            UidHelper::dec2bin('2722258935367507707706996859454145691647'),
        );
    }

    public function testDec2BinConversionAround128BitWithGmp(): void
    {
        if (!extension_loaded('gmp')) {
            self::markTestSkipped('GMP extension is required to run this test.');
        }

        self::assertSame(
            str_repeat('1', 125),
            UidHelper::dec2bin('42535295865117307932921825928971026431'),
        );

        self::assertSame(
            str_repeat('1', 126),
            UidHelper::dec2bin('85070591730234615865843651857942052863'),
        );

        self::assertSame(
            str_repeat('1', 127),
            UidHelper::dec2bin('170141183460469231731687303715884105727'),
        );

        self::assertSame(
            str_repeat('1', 128),
            UidHelper::dec2bin('340282366920938463463374607431768211455'),
        );

        self::assertSame(
            str_repeat('1', 129),
            UidHelper::dec2bin('680564733841876926926749214863536422911'),
        );

        self::assertSame(
            str_repeat('1', 130),
            UidHelper::dec2bin('1361129467683753853853498429727072845823'),
        );

        self::assertSame(
            str_repeat('1', 131),
            UidHelper::dec2bin('2722258935367507707706996859454145691647'),
        );
    }

    public function testDec2BinConversionIntegerBoundary(): void
    {
        // 9 digits = requires 30 bits
        self::assertSame(
            '111011100110101100100111111111',
            UidHelper::dec2bin(str_repeat('9', 9)),
        );

        // 10 digits = requires 34 bits
        self::assertSame(
            '1001010100000010111110001111111111',
            UidHelper::dec2bin(str_repeat('9', 10)),
        );

        // 18 digits = requires 60 bits
        self::assertSame(
            '110111100000101101101011001110100111011000111111111111111111',
            UidHelper::dec2bin(str_repeat('9', 18)),
        );

        // 19 digits = requires 64 bits
        self::assertSame(
            '1000101011000111001000110000010010001001111001111111111111111111',
            UidHelper::dec2bin(str_repeat('9', 19)),
        );

        // 20 digits = requires 67 bits
        self::assertSame(
            '1010110101111000111010111100010110101100011000011111111111111111111',
            UidHelper::dec2bin(str_repeat('9', 20)),
        );
    }

    public function testDec2BinEmptyInputThrowsException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Input string is not a decimal number: ');

        UidHelper::dec2bin('');
    }

    public function testDec2BinInvalidInputThrowsException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Input string is not a decimal number: 12345Test');

        UidHelper::dec2bin('12345Test');
    }

    public function testBin2DecConversion(): void
    {
        self::assertSame(
            '0',
            UidHelper::bin2dec('0'),
        );

        self::assertSame(
            '42',
            UidHelper::bin2dec('101010'),
        );

        self::assertSame(
            '1337',
            UidHelper::bin2dec('10100111001'),
        );
    }

    public function testBin2DecConversionAround32BitWithoutGmp(): void
    {
        global $mock_extension_loaded;
        $mock_extension_loaded = function ($name) {
            if (mb_strtolower($name) === 'gmp') {
                return false;
            }
            return \extension_loaded($name);
        };

        // 31 bits
        self::assertSame(
            '2147483647',
            UidHelper::bin2dec(str_repeat('1', 31)),
        );

        // 31 bits with leading zeros
        self::assertSame(
            '536870911',
            UidHelper::bin2dec('00' . str_repeat('1', 29)),
        );

        // 32 bits
        self::assertSame(
            '4294967295',
            UidHelper::bin2dec(str_repeat('1', 32)),
        );

        // 32 bits with leading zeros
        self::assertSame(
            '1073741823',
            UidHelper::bin2dec('00' . str_repeat('1', 30)),
        );

        // 33 bits
        self::assertSame(
            '8589934591',
            UidHelper::bin2dec(str_repeat('1', 33)),
        );

        // 33 bits with leading zeros
        self::assertSame(
            '2147483647',
            UidHelper::bin2dec('00' . str_repeat('1', 31)),
        );
    }

    public function testBin2DecConversionAround32BitWithGmp(): void
    {
        if (!extension_loaded('gmp')) {
            self::markTestSkipped('GMP extension is required to run this test.');
        }

        // 31 bits
        self::assertSame(
            '2147483647',
            UidHelper::bin2dec(str_repeat('1', 31)),
        );

        // 31 bits with leading zeros
        self::assertSame(
            '536870911',
            UidHelper::bin2dec('00' . str_repeat('1', 29)),
        );

        // 32 bits
        self::assertSame(
            '4294967295',
            UidHelper::bin2dec(str_repeat('1', 32)),
        );

        // 32 bits with leading zeros
        self::assertSame(
            '1073741823',
            UidHelper::bin2dec('00' . str_repeat('1', 30)),
        );

        // 33 bits
        self::assertSame(
            '8589934591',
            UidHelper::bin2dec(str_repeat('1', 33)),
        );

        // 33 bits with leading zeros
        self::assertSame(
            '2147483647',
            UidHelper::bin2dec('00' . str_repeat('1', 31)),
        );
    }

    public function testBin2DecConversionAround64BitWithoutGmp(): void
    {
        global $mock_extension_loaded;
        $mock_extension_loaded = function ($name) {
            if (mb_strtolower($name) === 'gmp') {
                return false;
            }
            return \extension_loaded($name);
        };

        // 63 bits
        self::assertSame(
            '9223372036854775807',
            UidHelper::bin2dec(str_repeat('1', 63)),
        );

        // 63 bits with leading zeros
        self::assertSame(
            '2305843009213693951',
            UidHelper::bin2dec('00' . str_repeat('1', 61)),
        );

        // 64 bits
        self::assertSame(
            '18446744073709551615',
            UidHelper::bin2dec(str_repeat('1', 64)),
        );

        // 64 bits with leading zeros
        self::assertSame(
            '4611686018427387903',
            UidHelper::bin2dec('00' . str_repeat('1', 62)),
        );

        // 65 bits
        self::assertSame(
            '36893488147419103231',
            UidHelper::bin2dec(str_repeat('1', 65)),
        );

        // 65 bits with leading zeros
        self::assertSame(
            '9223372036854775807',
            UidHelper::bin2dec('00' . str_repeat('1', 63)),
        );
    }

    public function testBin2DecConversionAround64BitWithGmp(): void
    {
        if (!extension_loaded('gmp')) {
            self::markTestSkipped('GMP extension is required to run this test.');
        }

        // 63 bits
        self::assertSame(
            '9223372036854775807',
            UidHelper::bin2dec(str_repeat('1', 63)),
        );

        // 63 bits with leading zeros
        self::assertSame(
            '2305843009213693951',
            UidHelper::bin2dec('00' . str_repeat('1', 61)),
        );

        // 64 bits
        self::assertSame(
            '18446744073709551615',
            UidHelper::bin2dec(str_repeat('1', 64)),
        );

        // 64 bits with leading zeros
        self::assertSame(
            '4611686018427387903',
            UidHelper::bin2dec('00' . str_repeat('1', 62)),
        );

        // 65 bits
        self::assertSame(
            '36893488147419103231',
            UidHelper::bin2dec(str_repeat('1', 65)),
        );

        // 65 bits with leading zeros
        self::assertSame(
            '9223372036854775807',
            UidHelper::bin2dec('00' . str_repeat('1', 63)),
        );
    }

    public function testBin2DecConversionAround128BitWithoutGmp(): void
    {
        global $mock_extension_loaded;
        $mock_extension_loaded = function ($name) {
            if (mb_strtolower($name) === 'gmp') {
                return false;
            }
            return \extension_loaded($name);
        };

        // 127 bits
        self::assertSame(
            '170141183460469231731687303715884105727',
            UidHelper::bin2dec(str_repeat('1', 127)),
        );

        // 127 bits with leading zeros
        self::assertSame(
            '42535295865117307932921825928971026431',
            UidHelper::bin2dec('00' . str_repeat('1', 125)),
        );

        // 128 bits
        self::assertSame(
            '340282366920938463463374607431768211455',
            UidHelper::bin2dec(str_repeat('1', 128)),
        );

        // 128 bits with leading zeros
        self::assertSame(
            '85070591730234615865843651857942052863',
            UidHelper::bin2dec('00' . str_repeat('1', 126)),
        );

        // 128 bits
        self::assertSame(
            '680564733841876926926749214863536422911',
            UidHelper::bin2dec(str_repeat('1', 129)),
        );

        // 129 bits with leading zeros
        self::assertSame(
            '170141183460469231731687303715884105727',
            UidHelper::bin2dec('00' . str_repeat('1', 127)),
        );
    }

    public function testBin2DecConversionAround128BitWithGmp(): void
    {
        if (!extension_loaded('gmp')) {
            self::markTestSkipped('GMP extension is required to run this test.');
        }

        // 127 bits
        self::assertSame(
            '170141183460469231731687303715884105727',
            UidHelper::bin2dec(str_repeat('1', 127)),
        );

        // 127 bits with leading zeros
        self::assertSame(
            '42535295865117307932921825928971026431',
            UidHelper::bin2dec('00' . str_repeat('1', 125)),
        );

        // 128 bits
        self::assertSame(
            '340282366920938463463374607431768211455',
            UidHelper::bin2dec(str_repeat('1', 128)),
        );

        // 128 bits with leading zeros
        self::assertSame(
            '85070591730234615865843651857942052863',
            UidHelper::bin2dec('00' . str_repeat('1', 126)),
        );

        // 128 bits
        self::assertSame(
            '680564733841876926926749214863536422911',
            UidHelper::bin2dec(str_repeat('1', 129)),
        );

        // 129 bits with leading zeros
        self::assertSame(
            '170141183460469231731687303715884105727',
            UidHelper::bin2dec('00' . str_repeat('1', 127)),
        );
    }

    public function testBin2DecEmptyInputThrowsException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Input string is not a binary string: ');

        UidHelper::bin2dec('');
    }

    public function testBin2DecInvalidInputThrowsException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Input string is not a binary string: 011001Test');

        UidHelper::bin2dec('011001Test');
    }

    public function testBin2HexConversion(): void
    {
        self::assertSame(
            '0',
            UidHelper::bin2hex('0'),
        );

        self::assertSame(
            '2a',
            UidHelper::bin2hex('101010'),
        );

        self::assertSame(
            'a735',
            UidHelper::bin2hex('1010011100110101'),
        );
    }

    public function testBin2HexConversionAround32BitWithoutGmp(): void
    {
        global $mock_extension_loaded;
        $mock_extension_loaded = function ($name) {
            if (mb_strtolower($name) === 'gmp') {
                return false;
            }
            return \extension_loaded($name);
        };

        // 31 bits
        self::assertSame(
            '7fffffff',
            UidHelper::bin2hex(str_repeat('1', 31)),
        );

        // 31 bits with leading zeros
        self::assertSame(
            '1fffffff',
            UidHelper::bin2hex('00' . str_repeat('1', 29)),
        );

        // 32 bits
        self::assertSame(
            'ffffffff',
            UidHelper::bin2hex(str_repeat('1', 32)),
        );

        // 32 bits with leading zeros
        self::assertSame(
            '3fffffff',
            UidHelper::bin2hex('00' . str_repeat('1', 30)),
        );

        // 33 bits
        self::assertSame(
            '1ffffffff',
            UidHelper::bin2hex(str_repeat('1', 33)),
        );

        // 33 bits with leading zeros
        self::assertSame(
            '7fffffff',
            UidHelper::bin2hex('00' . str_repeat('1', 31)),
        );
    }

    public function testBin2HexConversionAround32BitWithGmp(): void
    {
        if (!extension_loaded('gmp')) {
            self::markTestSkipped('GMP extension is required to run this test.');
        }

        // 31 bits
        self::assertSame(
            '7fffffff',
            UidHelper::bin2hex(str_repeat('1', 31)),
        );

        // 31 bits with leading zeros
        self::assertSame(
            '1fffffff',
            UidHelper::bin2hex('00' . str_repeat('1', 29)),
        );

        // 32 bits
        self::assertSame(
            'ffffffff',
            UidHelper::bin2hex(str_repeat('1', 32)),
        );

        // 32 bits with leading zeros
        self::assertSame(
            '3fffffff',
            UidHelper::bin2hex('00' . str_repeat('1', 30)),
        );

        // 33 bits
        self::assertSame(
            '1ffffffff',
            UidHelper::bin2hex(str_repeat('1', 33)),
        );

        // 33 bits with leading zeros
        self::assertSame(
            '7fffffff',
            UidHelper::bin2hex('00' . str_repeat('1', 31)),
        );
    }

    public function testBin2HexConversionAround64BitWithoutGmp(): void
    {
        global $mock_extension_loaded;
        $mock_extension_loaded = function ($name) {
            if (mb_strtolower($name) === 'gmp') {
                return false;
            }
            return \extension_loaded($name);
        };

        // 63 bits
        self::assertSame(
            '7fffffffffffffff',
            UidHelper::bin2hex(str_repeat('1', 63)),
        );

        // 63 bits with leading zeros
        self::assertSame(
            '1fffffffffffffff',
            UidHelper::bin2hex('00' . str_repeat('1', 61)),
        );

        // 64 bits
        self::assertSame(
            'ffffffffffffffff',
            UidHelper::bin2hex(str_repeat('1', 64)),
        );

        // 64 bits with leading zeros
        self::assertSame(
            '3fffffffffffffff',
            UidHelper::bin2hex('00' . str_repeat('1', 62)),
        );

        // 65 bits
        self::assertSame(
            '1ffffffffffffffff',
            UidHelper::bin2hex(str_repeat('1', 65)),
        );

        // 65 bits with leading zeros
        self::assertSame(
            '7fffffffffffffff',
            UidHelper::bin2hex('00' . str_repeat('1', 63)),
        );
    }

    public function testBin2HexConversionAround64BitWithGmp(): void
    {
        if (!extension_loaded('gmp')) {
            self::markTestSkipped('GMP extension is required to run this test.');
        }

        // 63 bits
        self::assertSame(
            '7fffffffffffffff',
            UidHelper::bin2hex(str_repeat('1', 63)),
        );

        // 63 bits with leading zeros
        self::assertSame(
            '1fffffffffffffff',
            UidHelper::bin2hex('00' . str_repeat('1', 61)),
        );

        // 64 bits
        self::assertSame(
            'ffffffffffffffff',
            UidHelper::bin2hex(str_repeat('1', 64)),
        );

        // 64 bits with leading zeros
        self::assertSame(
            '3fffffffffffffff',
            UidHelper::bin2hex('00' . str_repeat('1', 62)),
        );

        // 65 bits
        self::assertSame(
            '1ffffffffffffffff',
            UidHelper::bin2hex(str_repeat('1', 65)),
        );

        // 65 bits with leading zeros
        self::assertSame(
            '7fffffffffffffff',
            UidHelper::bin2hex('00' . str_repeat('1', 63)),
        );
    }

    public function testBin2HexConversionAround128BitWithoutGmp(): void
    {
        global $mock_extension_loaded;
        $mock_extension_loaded = function ($name) {
            if (mb_strtolower($name) === 'gmp') {
                return false;
            }
            return \extension_loaded($name);
        };

        // 127 bits
        self::assertSame(
            '7fffffffffffffffffffffffffffffff',
            UidHelper::bin2hex(str_repeat('1', 127)),
        );

        // 127 bits with leading zeros
        self::assertSame(
            '1fffffffffffffffffffffffffffffff',
            UidHelper::bin2hex('00' . str_repeat('1', 125)),
        );

        // 128 bits
        self::assertSame(
            'ffffffffffffffffffffffffffffffff',
            UidHelper::bin2hex(str_repeat('1', 128)),
        );

        // 128 bits with leading zeros
        self::assertSame(
            '3fffffffffffffffffffffffffffffff',
            UidHelper::bin2hex('00' . str_repeat('1', 126)),
        );

        // 128 bits
        self::assertSame(
            '1ffffffffffffffffffffffffffffffff',
            UidHelper::bin2hex(str_repeat('1', 129)),
        );

        // 129 bits with leading zeros
        self::assertSame(
            '7fffffffffffffffffffffffffffffff',
            UidHelper::bin2hex('00' . str_repeat('1', 127)),
        );
    }

    public function testBin2HexConversionAround128BitWithGmp(): void
    {
        if (!extension_loaded('gmp')) {
            self::markTestSkipped('GMP extension is required to run this test.');
        }

        // 127 bits
        self::assertSame(
            '7fffffffffffffffffffffffffffffff',
            UidHelper::bin2hex(str_repeat('1', 127)),
        );

        // 127 bits with leading zeros
        self::assertSame(
            '1fffffffffffffffffffffffffffffff',
            UidHelper::bin2hex('00' . str_repeat('1', 125)),
        );

        // 128 bits
        self::assertSame(
            'ffffffffffffffffffffffffffffffff',
            UidHelper::bin2hex(str_repeat('1', 128)),
        );

        // 128 bits with leading zeros
        self::assertSame(
            '3fffffffffffffffffffffffffffffff',
            UidHelper::bin2hex('00' . str_repeat('1', 126)),
        );

        // 128 bits
        self::assertSame(
            '1ffffffffffffffffffffffffffffffff',
            UidHelper::bin2hex(str_repeat('1', 129)),
        );

        // 129 bits with leading zeros
        self::assertSame(
            '7fffffffffffffffffffffffffffffff',
            UidHelper::bin2hex('00' . str_repeat('1', 127)),
        );
    }

    public function testBin2HexEmptyInputThrowsException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Input string is not a binary string: ');

        UidHelper::bin2hex('');
    }

    public function testBin2HexInvalidInputThrowsException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Input string is not a binary string: 011001Test');

        UidHelper::bin2hex('011001Test');
    }

    public function testRandomBytesAsBinaryUsingMtRand(): void
    {
        global $mock_function_exists;
        $mock_function_exists = function ($function_name) {
            if (in_array(mb_strtolower($function_name), ['random_bytes', 'openssl_random_pseudo_bytes'], true)) {
                return false;
            }
            return \function_exists($function_name);
        };

        self::assertMatchesRegularExpression(
            '=^[01]{8}$=',
            UidHelper::randomBytesAsBinary(1),
        );

        self::assertMatchesRegularExpression(
            '=^[01]{24}$=',
            UidHelper::randomBytesAsBinary(3),
        );

        self::assertMatchesRegularExpression(
            '=^[01]{80}$=',
            UidHelper::randomBytesAsBinary(10),
        );
    }

    public function testRandomBytesAsBinaryUsingOpensslRandomPseudoBytes(): void
    {
        if (!function_exists('openssl_random_pseudo_bytes')) {
            self::markTestSkipped('openssl_random_pseudo_bytes() is required to run this test.');
        }

        global $mock_function_exists;
        $mock_function_exists = function ($function_name) {
            if (mb_strtolower($function_name) === 'random_bytes') {
                return false;
            }
            return \function_exists($function_name);
        };

        self::assertMatchesRegularExpression(
            '=^[01]{8}$=',
            UidHelper::randomBytesAsBinary(1),
        );

        self::assertMatchesRegularExpression(
            '=^[01]{24}$=',
            UidHelper::randomBytesAsBinary(3),
        );

        self::assertMatchesRegularExpression(
            '=^[01]{80}$=',
            UidHelper::randomBytesAsBinary(10),
        );
    }

    public function testRandomBytesAsBinaryUsingRandomBytes(): void
    {
        if (!function_exists('random_bytes')) {
            self::markTestSkipped('random_bytes() is required to run this test.');
        }

        self::assertMatchesRegularExpression(
            '=^[01]{8}$=',
            UidHelper::randomBytesAsBinary(1),
        );

        self::assertMatchesRegularExpression(
            '=^[01]{24}$=',
            UidHelper::randomBytesAsBinary(3),
        );

        self::assertMatchesRegularExpression(
            '=^[01]{80}$=',
            UidHelper::randomBytesAsBinary(10),
        );
    }

    public function testRandomCharsOfAlphabetReturnsFromGivenAlphabetUsingMtRand(): void
    {
        global $mock_function_exists;
        $mock_function_exists = function ($function_name) {
            if (mb_strtolower($function_name) === 'random_int') {
                return false;
            }
            return \function_exists($function_name);
        };

        self::assertSame(
            '',
            UidHelper::randomCharsOfAlphabet('abcdef', -10),
        );

        self::assertSame(
            '',
            UidHelper::randomCharsOfAlphabet('abcdef', 0),
        );

        self::assertMatchesRegularExpression(
            '=^[abcdef]{4}$=',
            UidHelper::randomCharsOfAlphabet('abcdef', 4),
        );

        self::assertMatchesRegularExpression(
            '=^[abcdefghij]{8}$=',
            UidHelper::randomCharsOfAlphabet('abcdefghij', 8),
        );

        self::assertMatchesRegularExpression(
            '=^[abcdefghij0123456789]{64}$=',
            UidHelper::randomCharsOfAlphabet('abcdefghij0123456789', 64),
        );
    }

    public function testRandomCharsOfAlphabetReturnsFromGivenAlphabetUsingRandomInt(): void
    {
        if (!function_exists('random_int')) {
            self::markTestSkipped('random_int() is required to run this test.');
        }

        self::assertSame(
            '',
            UidHelper::randomCharsOfAlphabet('abcdef', -10),
        );

        self::assertSame(
            '',
            UidHelper::randomCharsOfAlphabet('abcdef', 0),
        );

        self::assertMatchesRegularExpression(
            '=^[abcdef]{4}$=',
            UidHelper::randomCharsOfAlphabet('abcdef', 4),
        );

        self::assertMatchesRegularExpression(
            '=^[abcdefghij]{8}$=',
            UidHelper::randomCharsOfAlphabet('abcdefghij', 8),
        );

        self::assertMatchesRegularExpression(
            '=^[abcdefghij0123456789]{64}$=',
            UidHelper::randomCharsOfAlphabet('abcdefghij0123456789', 64),
        );
    }

    public function testRandomCharsOfAlphabetEmptyStringThrowsException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Alphabet must not be empty.');

        UidHelper::randomCharsOfAlphabet('', 14);
    }

    public function testDetermineNumberOfBitsToMapAlphabetCalculation(): void
    {
        self::assertSame(
            1,
            UidHelper::determineNumberOfBitsToMapAlphabet('ab'),
        );

        self::assertSame(
            1,
            UidHelper::determineNumberOfBitsToMapAlphabet('abc'),
        );

        self::assertSame(
            2,
            UidHelper::determineNumberOfBitsToMapAlphabet('abcd'),
        );

        self::assertSame(
            2,
            UidHelper::determineNumberOfBitsToMapAlphabet('abcdef'),
        );

        self::assertSame(
            3,
            UidHelper::determineNumberOfBitsToMapAlphabet('abcdefgh'),
        );
    }

    public function testDetermineNumberOfBitsToMapAlphabetEmptyAlphabetThrowsException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Alphabet must contain at least 2 characters: ');

        UidHelper::determineNumberOfBitsToMapAlphabet('');
    }

    public function testDetermineNumberOfBitsToMapAlphabetTooShortAlphabetThrowsException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Alphabet must contain at least 2 characters: a');

        UidHelper::determineNumberOfBitsToMapAlphabet('a');
    }

    public function testConvertBinaryToAlphabetConversionWithMatchingLength(): void
    {
        self::assertSame(
            'cdab',
            UidHelper::convertBinaryToAlphabet('10110001', 'abcd'),
        );

        self::assertSame(
            'da',
            UidHelper::convertBinaryToAlphabet('011000', 'abcdefgh'),
        );

        self::assertSame(
            'ga5',
            UidHelper::convertBinaryToAlphabet('011000001101', 'abcdefgh01234567'),
        );
    }

    public function testConvertBinaryToAlphabetConversionWithBinaryMismatch(): void
    {
        // 1 bit short
        self::assertSame(
            'adab',
            UidHelper::convertBinaryToAlphabet('0110001', 'abcd'),
        );

        // 2 bits short
        self::assertSame(
            'bg',
            UidHelper::convertBinaryToAlphabet('1110', 'abcdefgh'),
        );

        // 3 bits short
        self::assertSame(
            'b05',
            UidHelper::convertBinaryToAlphabet('110001101', 'abcdefgh01234567'),
        );
    }

    public function testConvertBinaryToAlphabetConversionWithAlphabetMismatch(): void
    {
        // 3 characters, working with 2 (1 bit)
        self::assertSame(
            'abbaaab',
            UidHelper::convertBinaryToAlphabet('0110001', 'abc'),
        );

        // 10 characters, working with 8 (3 bits)
        self::assertSame(
            'bg',
            UidHelper::convertBinaryToAlphabet('001110', 'abcdefghij'),
        );

        // 30 characters, working with 16 (4 bits)
        self::assertSame(
            'bin',
            UidHelper::convertBinaryToAlphabet('000110001101', 'abcdefghijklmnopqrst0123456789'),
        );
    }

    public function testConvertBinaryToAlphabetConversionWithBinaryAndAlphabetMismatch(): void
    {
        // 7 characters, working with 4 (2 bits)
        self::assertSame(
            'bd',
            UidHelper::convertBinaryToAlphabet('111', 'abcdefg'),
        );

        // 10 characters, working with 8 (3 bits)
        self::assertSame(
            'bg',
            UidHelper::convertBinaryToAlphabet('1110', 'abcdefghij'),
        );

        // 30 characters, working with 16 (4 bits)
        self::assertSame(
            'bin',
            UidHelper::convertBinaryToAlphabet('110001101', 'abcdefghijklmnopqrst0123456789'),
        );
    }

    public function testConvertBinaryToAlphabetConversionWithLargeAlphabet(): void
    {
        self::assertSame(
            'XX',
            UidHelper::convertBinaryToAlphabet(str_repeat('1', 32), str_repeat('X', 1048576)),
        );
    }

    public function testConvertBinaryToAlphabetEmptyBinaryThrowsException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Input string is not a binary string: ');

        UidHelper::convertBinaryToAlphabet('', '');
    }

    public function testConvertBinaryToAlphabetInvalidBinaryThrowsException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Input string is not a binary string: 011001Test');

        UidHelper::convertBinaryToAlphabet('011001Test', '');
    }

    public function testConvertBinaryToAlphabetEmptyAlphabetThrowsException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Alphabet must contain at least 2 characters: ');

        UidHelper::convertBinaryToAlphabet('011001', '');
    }

    public function testConvertBinaryToAlphabetTooShortAlphabetThrowsException(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Alphabet must contain at least 2 characters: a');

        UidHelper::convertBinaryToAlphabet('011001', 'a');
    }
}
