<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\UniqueID\Unit;

use BjoernGoetschke\UniqueID\UniqidUidGenerator;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class UniqidGeneratorTest extends TestCase
{
    public function testRandomUid(): void
    {
        $generator = new UniqidUidGenerator();

        $uid = $generator->generate();

        self::assertNotEmpty($uid);

        self::assertSame(13, mb_strlen($uid));
    }

    public function testMoreEntropy(): void
    {
        $generator = new UniqidUidGenerator();

        $uid = $generator->generate(16);

        self::assertNotEmpty($uid);

        self::assertSame(23, mb_strlen($uid));
    }

    public function testMinimumLengthTooHigh(): void
    {
        $generator = new UniqidUidGenerator();

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Unable to generate an identifier with a minimum length of 32 characters.');

        $generator->generate(32);
    }

    public function testMaximumLengthTooLow(): void
    {
        $generator = new UniqidUidGenerator();

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Unable to generate an identifier with a maximum length of 6 characters.');

        $generator->generate(0, 6);
    }

    public function testClone(): void
    {
        $generator1 = new UniqidUidGenerator();
        $generator2 = clone $generator1;

        self::assertNotSame($generator1, $generator2);
    }

    public function testSerialize(): void
    {
        $generator1 = new UniqidUidGenerator();
        $generator2 = unserialize(serialize($generator1));

        self::assertInstanceOf(
            UniqidUidGenerator::class,
            $generator2,
        );

        self::assertNotSame($generator1, $generator2);
    }
}
