<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\UniqueID\Unit;

use BjoernGoetschke\UniqueID\CuidUidGenerator;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use ReflectionClass;

final class CuidGeneratorTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        resetMocks();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        resetMocks();
    }

    public function testClone(): void
    {
        $generator1 = new CuidUidGenerator();
        $generator2 = clone $generator1;

        self::assertNotSame($generator1, $generator2);
    }

    public function testSerialize(): void
    {
        $generator1 = new CuidUidGenerator();
        $generator2 = unserialize(serialize($generator1));

        self::assertInstanceOf(
            CuidUidGenerator::class,
            $generator2,
        );

        self::assertNotSame($generator1, $generator2);
    }

    public function testGenerateRequestedTooShortCuidException(): void
    {
        $generator = new CuidUidGenerator();

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage(
            'Length specification does not allow a collision-resistant id.',
        );

        $generator->generate(0, 5);
    }

    public function testGenerateCorrectCuidFullFormat(): void
    {
        $generator = new CuidUidGenerator();
        $uid = $generator->generate();

        self::assertMatchesRegularExpression(
            '=^c[0-9a-z]{17,}$=',
            $uid,
        );
    }

    public function testGenerateCorrectCuidFullFormatWithoutRandomIntFunction(): void
    {
        global $mock_function_exists;
        $mock_function_exists = function ($function_name) {
            if (mb_strtolower($function_name) === 'random_int') {
                return false;
            }
            return \function_exists($function_name);
        };

        $generator = new CuidUidGenerator();
        $uid = $generator->generate();

        self::assertMatchesRegularExpression(
            '=^c[0-9a-z]{17,}$=',
            $uid,
        );
    }

    public function testGenerateCorrectCuidSlugFormat(): void
    {
        $generator = new CuidUidGenerator();
        $uid = $generator->generate(0, 10);

        self::assertMatchesRegularExpression(
            '=^[0-9a-z]{7,10}$=',
            $uid,
        );
    }

    public function testGenerateCorrectCuidSlugFormatWithoutRandomIntFunction(): void
    {
        global $mock_function_exists;
        $mock_function_exists = function ($function_name) {
            if (mb_strtolower($function_name) === 'random_int') {
                return false;
            }
            return \function_exists($function_name);
        };

        $generator = new CuidUidGenerator();
        $uid = $generator->generate(0, 10);

        self::assertMatchesRegularExpression(
            '=^[0-9a-z]{7,10}$=',
            $uid,
        );
    }

    public function testMinimumLengthTooHigh(): void
    {
        $generator = new CuidUidGenerator();

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Unable to generate an identifier with a minimum length of 1024 characters.');

        $generator->generate(1024);
    }

    public function testMaximumLengthTooLow(): void
    {
        $generator = new CuidUidGenerator();

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Unable to generate an identifier with a maximum length of 11 characters.');

        $generator->generate(0, 11);
    }

    public function testCounterWraparound(): void
    {
        $class = new ReflectionClass(CuidUidGenerator::class);
        $class->setStaticPropertyValue('counter', 1679616);

        self::assertGreaterThan(1, $class->getStaticPropertyValue('counter'));

        $generator = new CuidUidGenerator();
        $uid = $generator->generate();

        self::assertNotEmpty($uid);
        self::assertLessThanOrEqual(1, $class->getStaticPropertyValue('counter'));
    }
}
