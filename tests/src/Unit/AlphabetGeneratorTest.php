<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\UniqueID\Unit;

use BjoernGoetschke\UniqueID\AlphabetUidGenerator;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class AlphabetGeneratorTest extends TestCase
{
    public function testDefaultAlphabet(): void
    {
        $generator = new AlphabetUidGenerator();

        self::assertSame(
            'abcdefghjkmnpqrstuvwxyz123456789',
            $generator->getAlphabet(),
        );
    }

    public function testGetSetAlphabet(): void
    {
        $generator = new AlphabetUidGenerator('abc123');

        self::assertSame('abc123', $generator->getAlphabet());

        $generator->setAlphabet('def456');

        self::assertSame('def456', $generator->getAlphabet());
    }

    public function testGenerateRandomUid(): void
    {
        $generator = new AlphabetUidGenerator();

        self::assertGreaterThan(
            0,
            mb_strlen($generator->generate()),
            'Generated uid must not be empty',
        );

        self::assertGreaterThan(
            0,
            mb_strlen($generator->generate(0, 10)),
            'Generated uid must not be empty',
        );

        self::assertGreaterThan(
            0,
            mb_strlen($generator->generate(5, 10)),
            'Generated uid must not be empty',
        );
    }

    public function testMinLengthBelowZero(): void
    {
        $generator = new AlphabetUidGenerator();

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Minimum length must not be less than 0.');

        $generator->generate(-1);
    }

    public function testMaxLengthBelowZero(): void
    {
        $generator = new AlphabetUidGenerator();

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Maximum length must not be less than 0.');

        $generator->generate(0, -1);
    }

    public function testRequireNonEmptyAlphabet(): void
    {
        $generator = new AlphabetUidGenerator();

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The alphabet must not be empty.');

        $generator->setAlphabet('');
    }

    public function testMinLengthGreaterThanMaxLength(): void
    {
        $generator = new AlphabetUidGenerator();

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Minimum length must not be greater than maximum length.');

        $generator->generate(5, 3);
    }

    public function testMinLengthCorrectionTo32(): void
    {
        $generator = new AlphabetUidGenerator();

        self::assertSame(
            32,
            mb_strlen($generator->generate(0, 0)),
            'Generated uid must be exactly 32 characters long.',
        );
    }

    public function testMinLengthWithoutMaxLength(): void
    {
        $generator = new AlphabetUidGenerator();

        self::assertSame(
            10,
            mb_strlen($generator->generate(10)),
            'Generated uid must be exactly 10 characters long.',
        );
    }

    public function testDefaultLengthInitialValue(): void
    {
        $generator = new AlphabetUidGenerator();

        self::assertSame(
            32,
            $generator->getDefaultLength(),
            'Default length must initially be set to 32.',
        );
    }

    public function testGetSetDefaultLength(): void
    {
        $generator = new AlphabetUidGenerator(null, 10);

        self::assertSame(
            10,
            $generator->getDefaultLength(),
            'Default length must be 10 after constructor.',
        );

        $generator->setDefaultLength(25);

        self::assertSame(
            25,
            $generator->getDefaultLength(),
            'Default length must be 25 after set.',
        );
    }

    public function testUseDefaultLengthIfMinMaxNotSpecified(): void
    {
        $generator = new AlphabetUidGenerator(null, 10);

        self::assertSame(
            10,
            mb_strlen($generator->generate(0, 0)),
            'Generated uid must be exactly 10 character long.',
        );
    }

    public function testSetDefaultLengthBelowOne(): void
    {
        $generator = new AlphabetUidGenerator();

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The default length must be greater than 0.');

        $generator->setDefaultLength(0);
    }

    public function testSetDefaultLengthBelowZero(): void
    {
        $generator = new AlphabetUidGenerator();

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The default length must be greater than 0.');

        $generator->setDefaultLength(-10);
    }

    public function testLengthBetweenMinAndMax(): void
    {
        $generator = new AlphabetUidGenerator();

        $length = mb_strlen($generator->generate(20, 40));

        self::assertGreaterThanOrEqual(
            20,
            $length,
            'Generated uid must be at least 20 characters long.',
        );

        self::assertLessThanOrEqual(
            40,
            $length,
            'Generated uid must not be more than 40 characters long.',
        );
    }

    public function testClone(): void
    {
        $generator1 = new AlphabetUidGenerator('123456789abc', 10);
        $generator2 = clone $generator1;

        self::assertNotSame(
            $generator1,
            $generator2,
        );

        self::assertSame(
            $generator1->getAlphabet(),
            $generator2->getAlphabet(),
        );

        self::assertSame(
            $generator1->getDefaultLength(),
            $generator2->getDefaultLength(),
        );
    }

    public function testSerialize(): void
    {
        $generator1 = new AlphabetUidGenerator('123456789abc', 10);
        $generator2 = unserialize(serialize($generator1));

        self::assertInstanceOf(
            AlphabetUidGenerator::class,
            $generator2,
        );

        self::assertNotSame(
            $generator1,
            $generator2,
        );

        self::assertSame(
            $generator1->getAlphabet(),
            $generator2->getAlphabet(),
        );

        self::assertSame(
            $generator1->getDefaultLength(),
            $generator2->getDefaultLength(),
        );
    }
}
