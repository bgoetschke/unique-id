<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\UniqueID\Unit;

use BjoernGoetschke\UniqueID\DatetimeUidGenerator;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class DatetimeGeneratorTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        resetMocks();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        resetMocks();
    }

    public function testClone(): void
    {
        $generator1 = new DatetimeUidGenerator();
        $generator2 = clone $generator1;

        self::assertNotSame($generator1, $generator2);
    }

    public function testSerialize(): void
    {
        $generator1 = new DatetimeUidGenerator();
        $generator2 = unserialize(serialize($generator1));

        self::assertInstanceOf(
            DatetimeUidGenerator::class,
            $generator2,
        );

        self::assertNotSame($generator1, $generator2);
    }

    public function testGenerateCorrectUidFormat(): void
    {
        $generator = new DatetimeUidGenerator();

        $uid = $generator->generate(10, 40);

        self::assertMatchesRegularExpression('=^[0-9a-z]{10,40}$=', $uid);
    }

    public function testGenerateExceptionOnTooLongIdentifier(): void
    {
        global $mock_microtime;
        $mock_microtime_output = [
            946684800,
        ];
        $mock_microtime = function ($get_as_float = false) use (&$mock_microtime_output) {
            self::assertTrue($get_as_float);
            self::assertNotCount(0, $mock_microtime_output);
            return array_shift($mock_microtime_output);
        };

        $generator = new DatetimeUidGenerator();

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Unable to generate an identifier with a maximum length of 10 characters.');

        try {
            $generator->generate(5, 10);
        } catch (InvalidArgumentException $e) {
            self::assertCount(
                0,
                $mock_microtime_output,
            );

            throw $e;
        }
    }

    public function testGenerateFixedTimestamp20000101(): void
    {
        global $mock_microtime;
        $mock_microtime_output = [
            946684800,
        ];
        $mock_microtime = function ($get_as_float = false) use (&$mock_microtime_output) {
            self::assertTrue($get_as_float);
            self::assertNotCount(0, $mock_microtime_output);
            return array_shift($mock_microtime_output);
        };

        $generator = new DatetimeUidGenerator();

        $uid = $generator->generate();

        self::assertSame(
            'b1tnzv5v5dcaa',
            $uid,
        );

        self::assertCount(
            0,
            $mock_microtime_output,
        );
    }

    public function testGenerateFixedTimestamp20000101ExpandedTo20Characters(): void
    {
        global $mock_microtime;
        $mock_microtime_output = [
            946684800,
        ];
        $mock_microtime = function ($get_as_float = false) use (&$mock_microtime_output) {
            self::assertTrue($get_as_float);
            self::assertNotCount(0, $mock_microtime_output);
            return array_shift($mock_microtime_output);
        };

        $generator = new DatetimeUidGenerator();

        $uid = $generator->generate(20, 30);

        self::assertSame(
            '0000000b1tnzv5v5dcaa',
            $uid,
        );

        self::assertCount(
            0,
            $mock_microtime_output,
        );
    }

    public function testMicrotimeGetsProperlyTruncated(): void
    {
        global $mock_microtime;
        $mock_microtime_output = [
            946684800.123456789,
        ];
        $mock_microtime = function ($get_as_float = false) use (&$mock_microtime_output) {
            self::assertTrue($get_as_float);
            self::assertNotCount(0, $mock_microtime_output);
            return array_shift($mock_microtime_output);
        };

        $generator = new DatetimeUidGenerator();

        $uid = $generator->generate();

        self::assertSame(
            'b1tnzv5v5drb4',
            $uid,
        );

        self::assertCount(
            0,
            $mock_microtime_output,
        );
    }

    public function testGenerateFixedTimestamp30000101(): void
    {
        global $mock_microtime;
        $mock_microtime_output = [
            32503680000,
        ];
        $mock_microtime = function ($get_as_float = false) use (&$mock_microtime_output) {
            self::assertTrue($get_as_float);
            self::assertNotCount(0, $mock_microtime_output);
            return array_shift($mock_microtime_output);
        };

        $generator = new DatetimeUidGenerator();

        $uid = $generator->generate();

        self::assertSame(
            'cwjuqkmguncaa',
            $uid,
        );

        self::assertCount(
            0,
            $mock_microtime_output,
        );
    }

    public function testGenerateFixedTimestamp60000101(): void
    {
        global $mock_microtime;
        $mock_microtime_output = [
            127174492800,
        ];
        $mock_microtime = function ($get_as_float = false) use (&$mock_microtime_output) {
            self::assertTrue($get_as_float);
            self::assertNotCount(0, $mock_microtime_output);
            return array_shift($mock_microtime_output);
        };

        $generator = new DatetimeUidGenerator();

        $uid = $generator->generate();

        self::assertSame(
            'fgucupzbxdcaa',
            $uid,
        );

        self::assertCount(
            0,
            $mock_microtime_output,
        );
    }

    public function testGenerateFixedTimestamp99990101(): void
    {
        global $mock_microtime;
        $mock_microtime_output = [
            253370764800,
        ];
        $mock_microtime = function ($get_as_float = false) use (&$mock_microtime_output) {
            self::assertTrue($get_as_float);
            self::assertNotCount(0, $mock_microtime_output);
            return array_shift($mock_microtime_output);
        };

        $generator = new DatetimeUidGenerator();

        $uid = $generator->generate();

        self::assertSame(
            'jyt68zy8azcaa',
            $uid,
        );

        self::assertCount(
            0,
            $mock_microtime_output,
        );
    }

    public function testGenerateFixedTimestampEndOf99991231(): void
    {
        global $mock_microtime;
        $mock_microtime_output = [
            253402300799.9999999999,
        ];
        $mock_microtime = function ($get_as_float = false) use (&$mock_microtime_output) {
            self::assertTrue($get_as_float);
            self::assertNotCount(0, $mock_microtime_output);
            return array_shift($mock_microtime_output);
        };

        $generator = new DatetimeUidGenerator();

        $uid = $generator->generate();

        self::assertSame(
            'jyu3pgttqdcaa',
            $uid,
        );

        self::assertCount(
            0,
            $mock_microtime_output,
        );
    }
}
