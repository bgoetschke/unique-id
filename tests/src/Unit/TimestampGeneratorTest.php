<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\UniqueID\Unit;

use BjoernGoetschke\UniqueID\TimestampUidGenerator;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class TimestampGeneratorTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        resetMocks();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        resetMocks();
    }

    public function testClone(): void
    {
        $generator1 = new TimestampUidGenerator();
        $generator2 = clone $generator1;

        self::assertNotSame($generator1, $generator2);
    }

    public function testSerialize(): void
    {
        $generator1 = new TimestampUidGenerator();
        $generator2 = unserialize(serialize($generator1));

        self::assertInstanceOf(
            TimestampUidGenerator::class,
            $generator2,
        );

        self::assertNotSame($generator1, $generator2);
    }

    public function testGenerateCorrectUidFormat(): void
    {
        $generator = new TimestampUidGenerator();

        $uid = $generator->generate(10, 40);

        self::assertMatchesRegularExpression('=^[0-9a-z]{10,40}$=', $uid);
    }

    public function testGenerateExceptionOnTooLongIdentifier(): void
    {
        global $mock_microtime;
        $mock_microtime_output = [
            946684800,
        ];
        $mock_microtime = function ($get_as_float = false) use (&$mock_microtime_output) {
            self::assertTrue($get_as_float);
            self::assertNotCount(0, $mock_microtime_output);
            return array_shift($mock_microtime_output);
        };

        $generator = new TimestampUidGenerator();

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Unable to generate an identifier with a maximum length of 5 characters.');

        try {
            $generator->generate(0, 5);
        } catch (InvalidArgumentException $e) {
            self::assertCount(
                0,
                $mock_microtime_output,
            );

            throw $e;
        }
    }

    public function testGenerateExpandChars(): void
    {
        global $mock_microtime;
        $mock_microtime_output = [
            0,
        ];
        $mock_microtime = function ($get_as_float = false) use (&$mock_microtime_output) {
            self::assertTrue($get_as_float);
            self::assertNotCount(0, $mock_microtime_output);
            return array_shift($mock_microtime_output);
        };

        $generator = new TimestampUidGenerator();

        $uid = $generator->generate(10, 15);

        self::assertSame(
            '000000000a',
            $uid,
        );

        self::assertCount(
            0,
            $mock_microtime_output,
        );
    }

    public function testGenerateFixedTimestamp20000101(): void
    {
        global $mock_microtime;
        $mock_microtime_output = [
            946684800,
        ];
        $mock_microtime = function ($get_as_float = false) use (&$mock_microtime_output) {
            self::assertTrue($get_as_float);
            self::assertNotCount(0, $mock_microtime_output);
            return array_shift($mock_microtime_output);
        };

        $generator = new TimestampUidGenerator();

        $uid = $generator->generate();

        self::assertSame(
            'czdg6u8paa',
            $uid,
        );

        self::assertCount(
            0,
            $mock_microtime_output,
        );
    }

    public function testMicrotimeGetsProperlyTruncated(): void
    {
        global $mock_microtime;
        $mock_microtime_output = [
            9876543210.123456789,
        ];
        $mock_microtime = function ($get_as_float = false) use (&$mock_microtime_output) {
            self::assertTrue($get_as_float);
            self::assertNotCount(0, $mock_microtime_output);
            return array_shift($mock_microtime_output);
        };

        $generator = new TimestampUidGenerator();

        $uid = $generator->generate();

        self::assertSame(
            '6cjt2sq9w4',
            $uid,
        );

        self::assertCount(
            0,
            $mock_microtime_output,
        );
    }

    public function testGenerateFixedTimestamp30000101(): void
    {
        global $mock_microtime;
        $mock_microtime_output = [
            32503680000,
        ];
        $mock_microtime = function ($get_as_float = false) use (&$mock_microtime_output) {
            self::assertTrue($get_as_float);
            self::assertNotCount(0, $mock_microtime_output);
            return array_shift($mock_microtime_output);
        };

        $generator = new TimestampUidGenerator();

        $uid = $generator->generate();

        self::assertSame(
            'c6pgfdasaaa',
            $uid,
        );

        self::assertCount(
            0,
            $mock_microtime_output,
        );
    }

    public function testGenerateFixedTimestamp60000101(): void
    {
        global $mock_microtime;
        $mock_microtime_output = [
            127174492800,
        ];
        $mock_microtime = function ($get_as_float = false) use (&$mock_microtime_output) {
            self::assertTrue($get_as_float);
            self::assertNotCount(0, $mock_microtime_output);
            return array_shift($mock_microtime_output);
        };

        $generator = new TimestampUidGenerator();

        $uid = $generator->generate();

        self::assertSame(
            'nkrrswpueaa',
            $uid,
        );

        self::assertCount(
            0,
            $mock_microtime_output,
        );
    }

    public function testGenerateFixedTimestamp99990101(): void
    {
        global $mock_microtime;
        $mock_microtime_output = [
            253370764800,
        ];
        $mock_microtime = function ($get_as_float = false) use (&$mock_microtime_output) {
            self::assertTrue($get_as_float);
            self::assertNotCount(0, $mock_microtime_output);
            return array_shift($mock_microtime_output);
        };

        $generator = new TimestampUidGenerator();

        $uid = $generator->generate();

        self::assertSame(
            'ztd79kw3taa',
            $uid,
        );

        self::assertCount(
            0,
            $mock_microtime_output,
        );
    }

    public function testGenerateFixedTimestampEndOf99991231(): void
    {
        global $mock_microtime;
        $mock_microtime_output = [
            253402300799.9999999999,
        ];
        $mock_microtime = function ($get_as_float = false) use (&$mock_microtime_output) {
            self::assertTrue($get_as_float);
            self::assertNotCount(0, $mock_microtime_output);
            return array_shift($mock_microtime_output);
        };

        $generator = new TimestampUidGenerator();

        $uid = $generator->generate();

        self::assertSame(
            'ztg32mhd6aa',
            $uid,
        );

        self::assertCount(
            0,
            $mock_microtime_output,
        );
    }
}
