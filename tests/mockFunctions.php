<?php

declare(strict_types=1);

namespace {

    global $mock_microtime;
    $mock_microtime = null;
    /**
     * @param bool $get_as_float
     * @return string|float
     */
    function mock_microtime(bool $get_as_float = false)
    {
        global $mock_microtime;
        $function = $mock_microtime;
        if (!($function instanceof Closure)) {
            $function = Closure::fromCallable('\microtime');
        }
        return $function($get_as_float);
    }

    global $mock_random_int;
    $mock_random_int = null;
    function mock_random_int(int $min, int $max): int
    {
        global $mock_random_int;
        $function = $mock_random_int;
        if (!($function instanceof Closure)) {
            $function = Closure::fromCallable('\random_int');
        }
        return $function($min, $max);
    }

    global $mock_extension_loaded;
    $mock_extension_loaded = null;
    function mock_extension_loaded(string $name): bool
    {
        global $mock_extension_loaded;
        $function = $mock_extension_loaded;
        if (!($function instanceof Closure)) {
            $function = Closure::fromCallable('\extension_loaded');
        }
        return $function($name);
    }

    global $mock_function_exists;
    $mock_function_exists = null;
    function mock_function_exists(string $function_name): bool
    {
        global $mock_function_exists;
        $function = $mock_function_exists;
        if (!($function instanceof Closure)) {
            $function = Closure::fromCallable('\function_exists');
        }
        return $function($function_name);
    }

    function resetMocks(): void
    {
        global $mock_microtime;
        $mock_microtime = null;
        global $mock_random_int;
        $mock_random_int = null;
        global $mock_extension_loaded;
        $mock_extension_loaded = null;
        global $mock_function_exists;
        $mock_function_exists = null;
    }
}

namespace BjoernGoetschke\UniqueID {

    /**
     * @param bool $get_as_float
     * @return string|float
     */
    function microtime(bool $get_as_float = false)
    {
        return \mock_microtime($get_as_float);
    }

    function random_int(int $min, int $max): int
    {
        return \mock_random_int($min, $max);
    }

    function extension_loaded(string $name): bool
    {
        return \mock_extension_loaded($name);
    }

    function function_exists(string $function_name): bool
    {
        return \mock_function_exists($function_name);
    }
}
