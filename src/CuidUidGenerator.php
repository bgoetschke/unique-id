<?php

declare(strict_types=1);

namespace BjoernGoetschke\UniqueID;

use InvalidArgumentException;

/**
 * Generates collision-resistant ids (CUIDs).
 *
 * @link https://usecuid.org/ CUID website
 * @api usage
 * @since 3.1
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
final class CuidUidGenerator implements UidGeneratorInterface
{
    private static int $counter = 0;

    private static ?string $fingerprint = null;

    public function generate(int $minLength = 0, int $maxLength = 0): string
    {
        if ($maxLength > 0 && $maxLength < 7) {
            throw new InvalidArgumentException(
                'Length specification does not allow a collision-resistant id.',
            );
        }

        $identifier = ($maxLength > 0 && $maxLength <= 10) ?
            $this->slug() :
            $this->cuid();

        return UidHelper::acceptIdentifier($identifier, $minLength, $maxLength);
    }

    /**
     * Generate a full length CUID.
     *
     * Always starts with the letter "c" and will have a length of at least 19 characters.
     *
     * Length depending on current timestamp, will be 25 characters until sometime in 2059.
     *
     * @return string
     * @api usage
     * @since 3.1
     */
    public function cuid(): string
    {
        return 'c' .
            $this->timestamp(false) .
            $this->counter(false) .
            $this->fingerprint(false) .
            $this->random(false) .
            $this->random(false);
    }

    /**
     * Generate a CUID slug.
     *
     * Will have a length of 7 - 10 characters.
     *
     * @return string
     * @api usage
     * @since 3.1
     */
    public function slug(): string
    {
        return $this->timestamp(true) .
            $this->counter(true) .
            $this->fingerprint(true) .
            $this->random(true);
    }

    private function timestamp(bool $short): string
    {
        // millisecond precision (*1000) of the timestamp to match the JavaScript reference implementation
        $return = base_convert((string)((int)microtime(true) * 1000), 10, 36);
        $return = str_pad($return, 2, '0', STR_PAD_LEFT);

        return $short ?
            substr($return, -2) :
            $return;
    }

    private function counter(bool $short): string
    {
        // 1679616 = 36^4 = pow(base, blockSize) in the JavaScript reference implementation
        if (self::$counter >= 1679616) {
            self::$counter = 0;
        }

        // 1679615 as base 36 = "zzzz", so the method will return a string with a length between 1 and 4
        $return = base_convert((string)self::$counter++, 10, 36);

        return $short ?
            $return :
            str_pad($return, 4, '0', STR_PAD_LEFT);
    }

    private function fingerprint(bool $short): string
    {
        if (self::$fingerprint === null) {
            $pid = base_convert((string)getmypid(), 10, 36);
            $pid = substr($pid, -2);
            $pid = str_pad($pid, 2, '0', STR_PAD_LEFT);

            $hostname = base_convert(
                (string)array_reduce(
                    str_split((string)gethostname()),
                    function (int $carry, string $item): int {
                        return $carry + ord($item);
                    },
                    strlen((string)gethostname()),
                ),
                10,
                36,
            );
            $hostname = substr($hostname, -2);
            $hostname = str_pad($hostname, 2, '0', STR_PAD_LEFT);

            self::$fingerprint = $pid . $hostname;
        }

        $return = self::$fingerprint;

        return $short ?
            substr($return, 0, 1) . substr($return, -1) :
            $return;
    }

    private function random(bool $short): string
    {
        // 1295 = 36^2 - 1 = 1-2 base36 characters
        // 1679615 = 36^4 - 1 = 1-4 base36 characters
        $max = ($short) ? 1295 : 1679615;
        $length = ($short) ? 2 : 4;

        $random = random_int(0, $max);

        return str_pad(
            base_convert((string)$random, 10, 36),
            $length,
            '0',
            STR_PAD_LEFT,
        );
    }
}
