<?php

declare(strict_types=1);

namespace BjoernGoetschke\UniqueID;

use BadMethodCallException;
use Closure;
use InvalidArgumentException;

/**
 * Checks generated unique identifiers for collisions using a specified callback.
 *
 * The callback must accept a single array of unique identifiers as an argument and must return a single element of
 * the passed array, the returned unique identifier will then be used as the final result of this generator.
 *
 * In the case that all identifiers in the array caused collisions, null/nothing should be returned to indicate that
 * none of the passed identifiers was acceptable as a final result.
 *
 * The number of identifiers in the list will vary ($collisionMultiplier) and the callback may be called multiple
 * times ($maximumTries) before this generator will give up and throw an exception.
 *
 * @api usage
 * @since 3.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
final class CheckCollisionUidGeneratorDecorator implements UidGeneratorInterface
{
    /**
     * The currently used generator.
     */
    private UidGeneratorInterface $generator;

    /**
     * The currently used callback.
     */
    private Closure $callback;

    /**
     * The number of initially generated unique identifiers.
     */
    private int $initialCount = 1;

    /**
     * The multiplier that is used when an collision occurs.
     */
    private int $collisionMultiplier = 2;

    /**
     * The maximum number of tries to find a unique identifier without a collision.
     */
    private int $maximumTries = 4;

    /**
     * Constructor.
     *
     * @param UidGeneratorInterface $generator
     *        The generator that should be used.
     * @param Closure $callback
     *        The callback that should be used.
     * @no-named-arguments
     */
    public function __construct(UidGeneratorInterface $generator, Closure $callback)
    {
        $this->setGenerator($generator);
        $this->setCallback($callback);
    }

    /**
     * Prevent serialize.
     *
     * @return array<string, mixed>
     * @codeCoverageIgnore
     */
    public function __serialize(): array
    {
        throw new BadMethodCallException('Cannot serialize ' . __CLASS__);
    }

    /**
     * Prevent unserialize.
     *
     * @param array<string, mixed> $data
     * @codeCoverageIgnore
     */
    public function __unserialize(array $data): void
    {
        throw new BadMethodCallException('Cannot unserialize ' . __CLASS__);
    }

    /**
     * Returns the currently used generator.
     *
     * @return UidGeneratorInterface
     * @api usage
     * @since 3.0
     */
    public function getGenerator(): UidGeneratorInterface
    {
        return $this->generator;
    }

    /**
     * Set the generator that should be used.
     *
     * @param UidGeneratorInterface $generator
     *        The generator that should be used.
     * @no-named-arguments
     * @api usage
     * @since 3.0
     */
    public function setGenerator(UidGeneratorInterface $generator): void
    {
        $this->generator = $generator;
    }

    /**
     * Returns the currently used callback.
     *
     * @return Closure
     * @api usage
     * @since 3.0
     */
    public function getCallback(): Closure
    {
        return $this->callback;
    }

    /**
     * Set the callback that should be used.
     *
     * @param Closure $callback
     *        The callback that should be used.
     * @no-named-arguments
     * @api usage
     * @since 3.0
     */
    public function setCallback(Closure $callback): void
    {
        $this->callback = $callback;
    }

    /**
     * Returns the number of initially generated unique identifiers.
     *
     * @return int
     * @api usage
     * @since 3.0
     */
    public function getInitialCount(): int
    {
        return $this->initialCount;
    }

    /**
     * Set the number of initially generated unique identifiers.
     *
     * @param int $initialCount
     *        The new number of initially generated unique identifiers.
     * @no-named-arguments
     * @api usage
     * @since 3.0
     */
    public function setInitialCount(int $initialCount): void
    {
        if ($initialCount < 1) {
            throw new InvalidArgumentException(
                'Number of initially generated unique identifiers must not be less than 1.',
            );
        }

        $this->initialCount = $initialCount;
    }

    /**
     * Returns the multiplier that is used when an collision occurs.
     *
     * @return int
     * @api usage
     * @since 3.0
     */
    public function getCollisionMultiplier(): int
    {
        return $this->collisionMultiplier;
    }

    /**
     * Set the multiplier that is used when an collision occurs.
     *
     * @param int $collisionMultiplier
     *        The new multiplier.
     * @no-named-arguments
     * @api usage
     * @since 3.0
     */
    public function setCollisionMultiplier(int $collisionMultiplier): void
    {
        if ($collisionMultiplier < 0) {
            throw new InvalidArgumentException('Collision multiplier must not be less than 0.');
        }

        $this->collisionMultiplier = $collisionMultiplier;
    }

    /**
     * Returns the maximum number of tries to find a unique identifier without a collision.
     *
     * @return int
     * @api usage
     * @since 3.0
     */
    public function getMaximumTries(): int
    {
        return $this->maximumTries;
    }

    /**
     * Set the maximum number of tries to find a unique identifier without a collision.
     *
     * @param int $maximumTries
     *        The new maximum number of tries.
     * @no-named-arguments
     * @api usage
     * @since 3.0
     */
    public function setMaximumTries(int $maximumTries): void
    {
        if ($maximumTries < 1) {
            throw new InvalidArgumentException('Maximum number of tries must not be less than 1.');
        }

        $this->maximumTries = $maximumTries;
    }

    public function generate(int $minLength = 0, int $maxLength = 0): string
    {
        $count = $this->initialCount;

        for ($try = 1; $try <= $this->maximumTries; $try++) {
            $list = [];
            for ($i = 1; $i <= $count; $i++) {
                $list[] = $this->generator->generate($minLength, $maxLength);
            }

            // Since call_user_func() can pass arguments only by value, there is no need to make sure $list is not
            // modified by the callback. If the callback defined the first argument to be passed by reference
            // (using the &-sign before the variable name) php will raise a warning.
            $identifier = call_user_func($this->callback, $list);

            if (in_array($identifier, $list, true)) {
                return $identifier;
            }

            $count *= $this->collisionMultiplier;
        }

        $msg = 'Failed to generate a unique identifier without a collision.';
        throw new InvalidArgumentException($msg);
    }
}
