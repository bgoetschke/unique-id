<?php

declare(strict_types=1);

namespace BjoernGoetschke\UniqueID;

/**
 * Generates unique identifier based on the current datetime.
 *
 * Identifiers contain lowercase characters and digits, omitting the characters i (I), l (L) and o (O).
 * The digit 0 (zero) is only used as filler at the beginning of the identifier to reach the requested minimum length.
 *
 * Length of the generated identifier at the end of year 9999 is 13 characters.
 *
 * @api usage
 * @since 3.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
final class DatetimeUidGenerator implements UidGeneratorInterface
{
    public function generate(int $minLength = 0, int $maxLength = 0): string
    {
        list($sec, $msec) = explode('.', number_format((float)microtime(true), 5, '.', ''));
        $binary = UidHelper::dec2bin(gmdate("YmdHis", (int)$sec) . $msec);

        // Omitting the following characters to avoid confusion: i, l, o, 0
        // It would be nice to omit the number 1 as well, but 32 characters are needed
        // to correctly use 5 bits per character during the conversion.
        // The number 0 is used to pad the identifier to the requested minimum length.
        $identifier = UidHelper::convertBinaryToAlphabet($binary, 'abcdefghjkmnpqrstuvwxyz123456789');
        $identifier = str_pad($identifier, $minLength, '0', STR_PAD_LEFT);

        return UidHelper::acceptIdentifier($identifier, $minLength, $maxLength);
    }
}
