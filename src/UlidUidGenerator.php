<?php

declare(strict_types=1);

namespace BjoernGoetschke\UniqueID;

use InvalidArgumentException;
use RuntimeException;

/**
 * Generates universally unique lexicographically sortable identifier (ULIDs).
 *
 * @link https://github.com/ulid/spec ULID specification
 * @api usage
 * @since 3.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
final class UlidUidGenerator implements UidGeneratorInterface
{
    public function generate(int $minLength = 0, int $maxLength = 0): string
    {
        if (($minLength > 0 && $minLength > 26) || ($maxLength > 0 && $maxLength < 26)) {
            throw new InvalidArgumentException(
                'Length specification does not allow an universally unique lexicographically sortable identifier.',
            );
        }

        // current timestamp as binary, pad to 48 bits
        $binary = UidHelper::dec2bin(number_format((float)microtime(true), 3, '', ''));
        $binary = str_pad($binary, 48, '0', STR_PAD_LEFT);

        if (strlen($binary) !== 48) {
            throw new RuntimeException(
                'Unable to generate an universally unique lexicographically sortable identifier, ' .
                'timestamp is not exactly 48 bits.',
            );
        }

        // random bits until the total length reaches 128 bit
        $binary .= UidHelper::randomBytesAsBinary(10);

        // Crockford Base32 alphabet: https://www.crockford.com/base32.html
        $identifier = UidHelper::convertBinaryToAlphabet($binary, '0123456789ABCDEFGHJKMNPQRSTVWXYZ');

        return UidHelper::acceptIdentifier($identifier, $minLength, $maxLength);
    }
}
