<?php

declare(strict_types=1);

namespace BjoernGoetschke\UniqueID;

use InvalidArgumentException;

/**
 * Internal helper methods for conversion and random number generation.
 *
 * @api internal
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
final class UidHelper
{
    /**
     * Private constructor.
     *
     * @codeCoverageIgnore
     */
    private function __construct()
    {
    }

    /**
     * Pad the specified string to a length that is divisible by the specified mod.
     *
     * @param string $input
     *        The string that should be padded, MUST NOT be a multibyte character set
     *        (since {@see str_pad()} is also not multibyte safe)
     * @param int $mod
     *        The mod the string should be divisible by.
     * @param string $padString
     *        The string that should be used for padding.
     * @param int $padType
     *        Valid pad type for {@see str_pad()}
     * @return string
     * @no-named-arguments
     * @api internal
     */
    public static function padStringToModLength(string $input, int $mod, string $padString, int $padType): string
    {
        $originalLength = strlen($input);
        $missingLength = ($mod - ($originalLength % $mod)) % $mod;
        $newLength = $originalLength + $missingLength;

        return str_pad($input, $newLength, $padString, $padType);
    }

    /**
     * Convert the specified decimal number to a binary number.
     *
     * @link http://www.exploringbinary.com/base-conversion-in-php-using-bcmath/
     * @param string $dec
     * @return string
     * @no-named-arguments
     * @api internal
     * @noinspection PhpComposerExtensionStubsInspection
     */
    public static function dec2bin(string $dec): string
    {
        if ($dec === '' || ltrim($dec, '0..9') !== '') {
            throw new InvalidArgumentException(sprintf('Input string is not a decimal number: %1$s', $dec));
        }

        $length = strlen($dec);
        if (
            ($length <= 9) || // 32-bit system can handle the number (9 digits = requires 30 bits)
            (PHP_INT_SIZE >= 8 && $length <= 18) // 64-bit system can handle the number (18 digits = requires 60 bits)
        ) {
            // using native methods because number is below platform integer limit
            return decbin((int)$dec);
        }

        if (extension_loaded('gmp')) {
            return gmp_strval(gmp_init($dec, 10), 2);
        }

        // bcmath fallback, not checked if extension is actually loaded
        $bin = '';
        do {
            $bin = bcmod($dec, '2') . $bin;
            $dec = bcdiv($dec, '2', 0);
        } while (bccomp($dec, '0'));
        return $bin;
    }

    /**
     * Convert the specified binary number to a decimal number.
     *
     * @link http://www.exploringbinary.com/base-conversion-in-php-using-bcmath/
     * @param string $bin
     * @return string
     * @no-named-arguments
     * @api internal
     * @noinspection PhpComposerExtensionStubsInspection
     */
    public static function bin2dec(string $bin): string
    {
        if ($bin === '' || ltrim($bin, '01') !== '') {
            throw new InvalidArgumentException(sprintf('Input string is not a binary string: %1$s', $bin));
        }

        $length = strlen($bin);
        if (
            ($length <= 31) || // 32-bit system can handle the number
            (PHP_INT_SIZE >= 8 && $length <= 63) // 64-bit system can handle the number
        ) {
            // using native methods because number is below platform integer limit
            return (string)bindec($bin);
        }

        if (extension_loaded('gmp')) {
            return gmp_strval(gmp_init($bin, 2), 10);
        }

        // bcmath fallback, not checked if extension is actually loaded
        $dec = '0';
        for ($i = 0; $i < $length; $i++) {
            $dec = bcmul($dec, '2', 0);
            $dec = bcadd($dec, $bin[$i], 0);
        }
        return $dec;
    }

    /**
     * Convert the specified binary number to a hexadecimal number.
     *
     * @param string $bin
     * @return string
     * @no-named-arguments
     * @api internal
     * @noinspection PhpComposerExtensionStubsInspection
     */
    public static function bin2hex(string $bin): string
    {
        if ($bin === '' || ltrim($bin, '01') !== '') {
            throw new InvalidArgumentException(sprintf('Input string is not a binary string: %1$s', $bin));
        }

        $length = strlen($bin);
        if (
            ($length <= 31) || // 32-bit system can handle the number
            (PHP_INT_SIZE >= 8 && $length <= 63) // 64-bit system can handle the number
        ) {
            // using native methods because number is below platform integer limit
            return dechex((int)bindec($bin));
        }

        if (extension_loaded('gmp')) {
            return gmp_strval(gmp_init($bin, 2), 16);
        }

        return self::convertBinaryToAlphabet(str_pad(ltrim($bin, '0'), 1, '0', STR_PAD_LEFT), '0123456789abcdef');
    }

    /**
     * Generate the specified number of random bytes and return its binary representation as string.
     *
     * @param int $numBytes
     *        Number of bytes to generate, at least 1.
     * @return string
     * @no-named-arguments
     * @api internal
     */
    public static function randomBytesAsBinary(int $numBytes): string
    {
        $numBytes = max(1, $numBytes);
        $bytes = [];

        if (function_exists('random_bytes')) {
            $bytes = (array)unpack('C*', random_bytes($numBytes));
        } elseif (function_exists('openssl_random_pseudo_bytes')) {
            $bytes = (array)unpack('C*', (string)openssl_random_pseudo_bytes($numBytes));
        } else {
            for ($i = 0; $i < $numBytes; $i++) {
                $bytes[] = random_int(0, 255);
            }
        }

        $bin = '';
        foreach ($bytes as $byte) {
            // using native methods because number will be max 255 (8 bit)
            $bin .= str_pad(decbin($byte), 8, '0', STR_PAD_LEFT);
        }
        return $bin;
    }

    /**
     * Returns the specified number of random characters of a string.
     *
     * @param string $alphabet
     *        String of available characters, can be a multibyte character set.
     * @param int $numChars
     *        The number of characters to select form the alphabet.
     * @return string
     * @no-named-arguments
     * @api internal
     */
    public static function randomCharsOfAlphabet(string $alphabet, int $numChars): string
    {
        $numChars = max(0, $numChars);
        $length = mb_strlen($alphabet);

        if ($length < 1) {
            throw new InvalidArgumentException('Alphabet must not be empty.');
        }

        $chars = [];
        $maxIndex = $length - 1;

        for ($i = 0; $i < $numChars; $i++) {
            $chars[] = random_int(0, $maxIndex);
        }

        $result = '';
        foreach ($chars as $index) {
            $result .= mb_substr($alphabet, $index, 1);
        }
        return $result;
    }

    /**
     * Determine that number of bits that can be used to map a binary string to the specified alphabet.
     *
     * The result is rounded down to a full integer, in case the number of characters in the alphabet is
     * not a power of 2, the last characters will not be mapped.
     *
     * @param string $alphabet
     *        The alphabet that should be mapped, can be a multibyte character set.
     * @return int Number of bits, at least 1
     * @psalm-return int<1, max>
     * @throws InvalidArgumentException In case the resulting number of bits is less than 1
     * @no-named-arguments
     * @api internal
     */
    public static function determineNumberOfBitsToMapAlphabet(string $alphabet): int
    {
        $alphabetLength = mb_strlen($alphabet);
        $numberOfBits = (int)floor(log($alphabetLength, 2));
        if ($numberOfBits < 1) {
            throw new InvalidArgumentException(sprintf('Alphabet must contain at least 2 characters: %1$s', $alphabet));
        }
        return $numberOfBits;
    }

    /**
     * Convert the specified binary string to characters of the specified alphabet.
     *
     * @param string $bin
     *        The binary string that should be converted.
     * @param string $alphabet
     *        The alphabet to convert to, can be a multibyte character set.
     * @return string
     * @no-named-arguments
     * @api internal
     */
    public static function convertBinaryToAlphabet(string $bin, string $alphabet): string
    {
        if ($bin === '' || ltrim($bin, '01') !== '') {
            throw new InvalidArgumentException(sprintf('Input string is not a binary string: %1$s', $bin));
        }

        $mod = self::determineNumberOfBitsToMapAlphabet($alphabet);
        $bin = self::padStringToModLength($bin, $mod, '0', STR_PAD_LEFT);

        $binChunks = str_split($bin, $mod);
        assert($binChunks !== false);
        $binChars = [];
        if ($mod <= 16) {
            // using native methods because number will be max 65535 (16 bit)
            foreach ($binChunks as $chunk) {
                $binChars[] = (int)bindec($chunk);
            }
        } else {
            foreach ($binChunks as $chunk) {
                $binChars[] = (int)self::bin2dec($chunk);
            }
        }

        $result = '';
        foreach ($binChars as $char) {
            $result .= mb_substr($alphabet, $char, 1);
        }
        return $result;
    }

    /**
     * Check the length of the specified identifier.
     *
     * @param string $identifier
     *        The identifier that should be checked.
     * @param int $minLength
     *        The minimum length of the identifier.
     * @param int $maxLength
     *        The maximum length of the identifier.
     * @return string
     * @throws InvalidArgumentException
     *         The length requirements could not be satisfied.
     * @no-named-arguments
     * @api internal
     */
    public static function acceptIdentifier(string $identifier, int $minLength, int $maxLength): string
    {
        $identifierLength = mb_strlen($identifier);

        if ($minLength > 0 && $identifierLength < $minLength) {
            $msg = sprintf(
                'Unable to generate an identifier with a minimum length of %1$d characters.',
                $minLength,
            );
            throw new InvalidArgumentException($msg);
        }

        if ($maxLength > 0 && $identifierLength > $maxLength) {
            $msg = sprintf(
                'Unable to generate an identifier with a maximum length of %1$d characters.',
                $maxLength,
            );
            throw new InvalidArgumentException($msg);
        }

        return $identifier;
    }
}
