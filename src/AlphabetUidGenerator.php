<?php

declare(strict_types=1);

namespace BjoernGoetschke\UniqueID;

use InvalidArgumentException;

/**
 * Generates unique identifier using characters from an alphabet.
 *
 * The identifier is generated using random characters from the specified alphabet. The default alphabet
 * contains 32 lowercase characters and digits, omitting the characters i (I), l (L) and o (O) and also
 * the digit 0 (zero).
 *
 * In case no minimum and maximum length are specified, the identifier will have the default length.
 *
 * In case only a minimum length is specified, the identifier will have that length.
 *
 * In case only a maximum length is specified, the identifier will have a random length
 * between 1 and the specified maximum length.
 *
 * In case both a minimum and a maximum length have been specified, the identifier will have a
 * random length between these two values.
 *
 * @api usage
 * @since 3.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
final class AlphabetUidGenerator implements UidGeneratorInterface
{
    /**
     * The chars that the uid can contain.
     *
     * Omitting the following chars to avoid confusion: i, l, o, 0
     */
    private string $alphabet = 'abcdefghjkmnpqrstuvwxyz123456789';

    /**
     * Length that will be used of no minimum length and maximum length are specified.
     *
     * Set to 32 to avoid unintentional generation of very short uids (was set to 1 before) when
     * no default length or minimum length is specified explicitly.
     */
    private int $defaultLength = 32;

    /**
     * Constructor.
     *
     * @param string|null $alphabet
     *        The alphabet that should be used.
     * @param int|null $defaultLength
     *        Length that will be used of no minimum length and maximum length are specified.
     * @no-named-arguments
     */
    public function __construct(?string $alphabet = null, ?int $defaultLength = null)
    {
        if ($alphabet !== null) {
            $this->setAlphabet($alphabet);
        }

        if ($defaultLength !== null) {
            $this->setDefaultLength($defaultLength);
        }
    }

    /**
     * @return array{alphabet: string, defaultLength: int}
     */
    public function __serialize(): array
    {
        return [
            'alphabet' => $this->alphabet,
            'defaultLength' => $this->defaultLength,
        ];
    }

    /**
     * @param array{alphabet: string, defaultLength: int} $data
     */
    public function __unserialize(array $data): void
    {
        $this->alphabet = $data['alphabet'];
        $this->defaultLength = $data['defaultLength'];
    }

    /**
     * Returns the currently used alphabet.
     *
     * @return string
     * @api usage
     * @since 3.0
     */
    public function getAlphabet(): string
    {
        return $this->alphabet;
    }

    /**
     * Set the alphabet that should be used.
     *
     * @param string $alphabet
     *        The alphabet that should be used.
     * @no-named-arguments
     * @api usage
     * @since 3.0
     */
    public function setAlphabet(string $alphabet): void
    {
        if (strlen($alphabet) < 1) {
            throw new InvalidArgumentException('The alphabet must not be empty.');
        }

        $this->alphabet = $alphabet;
    }

    /**
     * Returns the length that will be used of no minimum length and maximum length are specified.
     *
     * @return int
     * @api usage
     * @since 3.0
     */
    public function getDefaultLength(): int
    {
        return $this->defaultLength;
    }

    /**
     * Set the length that will be used of no minimum length and maximum length are specified.
     *
     * @param int $defaultLength
     *        Length that will be used of no minimum length and maximum length are specified.
     * @no-named-arguments
     * @api usage
     * @since 3.0
     */
    public function setDefaultLength(int $defaultLength): void
    {
        if ($defaultLength <= 0) {
            throw new InvalidArgumentException('The default length must be greater than 0.');
        }

        $this->defaultLength = $defaultLength;
    }

    public function generate(int $minLength = 0, int $maxLength = 0): string
    {
        if ($minLength < 0) {
            throw new InvalidArgumentException('Minimum length must not be less than 0.');
        }

        if ($maxLength < 0) {
            throw new InvalidArgumentException('Maximum length must not be less than 0.');
        }

        if ($minLength > $maxLength && $maxLength > 0) {
            throw new InvalidArgumentException('Minimum length must not be greater than maximum length.');
        }

        $length = $minLength;

        if ($minLength < 1 && $maxLength < 1) {
            $length = $this->defaultLength;
        } elseif ($minLength < 1) {
            $length = random_int(1, $maxLength);
        } elseif ($maxLength > 0) {
            $length = random_int($minLength, $maxLength);
        }

        $identifier = UidHelper::randomCharsOfAlphabet($this->alphabet, $length);

        return UidHelper::acceptIdentifier($identifier, $minLength, $maxLength);
    }
}
