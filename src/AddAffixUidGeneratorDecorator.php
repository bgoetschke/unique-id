<?php

declare(strict_types=1);

namespace BjoernGoetschke\UniqueID;

use InvalidArgumentException;

/**
 * Adds an prefix and/or suffix to generated unique identifiers.
 *
 * @api usage
 * @since 3.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
final class AddAffixUidGeneratorDecorator implements UidGeneratorInterface
{
    /**
     * The currently used generator.
     */
    private UidGeneratorInterface $generator;

    /**
     * The currently used prefix.
     */
    private string $prefix;

    /**
     * The currently used suffix.
     */
    private string $suffix;

    /**
     * Constructor.
     *
     * @param UidGeneratorInterface $generator
     *        The generator that should be used.
     * @param string $prefix
     *        The prefix that should be used.
     * @param string $suffix
     *        The suffix that should be used.
     * @no-named-arguments
     */
    public function __construct(UidGeneratorInterface $generator, string $prefix = '', string $suffix = '')
    {
        $this->setGenerator($generator);
        $this->setPrefix($prefix);
        $this->setSuffix($suffix);
    }

    /**
     * @return array{generator: UidGeneratorInterface, prefix: string, suffix: string}
     */
    public function __serialize(): array
    {
        return [
            'generator' => $this->generator,
            'prefix' => $this->prefix,
            'suffix' => $this->suffix,
        ];
    }

    /**
     * @param array{generator: UidGeneratorInterface, prefix: string, suffix: string} $data
     */
    public function __unserialize(array $data): void
    {
        $this->generator = $data['generator'];
        $this->prefix = $data['prefix'];
        $this->suffix = $data['suffix'];
    }

    /**
     * Returns the currently used prefix.
     *
     * @return string
     * @api usage
     * @since 3.0
     */
    public function getPrefix(): string
    {
        return $this->prefix;
    }

    /**
     * Set a new prefix.
     *
     * @param string $prefix
     *        The prefix that should be used.
     * @throws InvalidArgumentException
     *         The specified suffix is invalid.
     * @no-named-arguments
     * @api usage
     * @since 3.0
     */
    public function setPrefix(string $prefix): void
    {
        if (!$this->checkAffix($prefix)) {
            throw new InvalidArgumentException(sprintf('Invalid prefix specified: %1$s', $prefix));
        }

        $this->prefix = $prefix;
    }

    /**
     * Returns the currently used suffix.
     *
     * @return string
     * @api usage
     * @since 3.0
     */
    public function getSuffix(): string
    {
        return $this->suffix;
    }

    /**
     * Set a new suffix.
     *
     * @param string $suffix
     *        The suffix that should be used.
     * @throws InvalidArgumentException
     *         The specified suffix is invalid.
     * @api usage
     * @since 3.0
     */
    public function setSuffix(string $suffix): void
    {
        if (!$this->checkAffix($suffix)) {
            throw new InvalidArgumentException(sprintf('Invalid suffix specified: %1$s', $suffix));
        }

        $this->suffix = $suffix;
    }

    /**
     * Returns the currently used generator.
     *
     * @return UidGeneratorInterface
     * @api usage
     * @since 3.0
     */
    public function getGenerator(): UidGeneratorInterface
    {
        return $this->generator;
    }

    /**
     * Set the generator that should be used.
     *
     * @param UidGeneratorInterface $generator
     *        The generator that should be used.
     * @no-named-arguments
     * @api usage
     * @since 3.0
     */
    public function setGenerator(UidGeneratorInterface $generator): void
    {
        $this->generator = $generator;
    }

    public function generate(int $minLength = 0, int $maxLength = 0): string
    {
        $affixLength = mb_strlen($this->prefix . $this->suffix);

        if ($maxLength > 0 && $affixLength >= $maxLength) {
            $msg = sprintf(
                'Affix length of %1$d exceeds maximum allowed length of %2$d.',
                $affixLength,
                $maxLength,
            );
            throw new InvalidArgumentException($msg);
        }

        $passMinLength = max(($minLength - $affixLength), 0);
        $passMaxLength = max(($maxLength - $affixLength), 0);

        $identifier = $this->generator->generate(
            $passMinLength,
            $passMaxLength,
        );
        $identifier = $this->prefix . $identifier . $this->suffix;

        return UidHelper::acceptIdentifier($identifier, $minLength, $maxLength);
    }

    /**
     * Check if the specified string is an valid affix.
     *
     * Returns true if the spring is an valid affix, otherwise false.
     *
     * Valid characters are: 0-9 a-z + - _ . * #
     *
     * @param string $affix
     *        The affix that should be checked.
     * @return bool
     * @no-named-arguments
     */
    private function checkAffix(string $affix): bool
    {
        return (bool)preg_match('=^[0-9a-z+\-_.*#]*$=', $affix);
    }
}
