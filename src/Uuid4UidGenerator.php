<?php

declare(strict_types=1);

namespace BjoernGoetschke\UniqueID;

use InvalidArgumentException;

/**
 * Generates universally unique identifier, version 4.
 *
 * @api usage
 * @since 3.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
final class Uuid4UidGenerator implements UidGeneratorInterface
{
    public function generate(int $minLength = 0, int $maxLength = 0): string
    {
        if (($minLength > 36) || ($maxLength > 0 && $maxLength < 36)) {
            throw new InvalidArgumentException(
                'Length specification does not allow an universally unique identifier.',
            );
        }

        // Uuid format: 00000000-0000-0000-0000-000000000000 (32 characters + 4 separaters, 16 bytes, 128 bit)
        //
        // [time-low (4 byte)]-[time-mid (2 bytes)]- // 6 bytes (0-5), bits 0 to 47
        // [time-high-and-version (2 bytes)]-        // 2 bytes (6-7), bits 48 to 63
        // [clock-seq-and-reserved (1 byte)]         // 1 byte  (8), bits 64 to 71
        // [clock-seq-low (1 byte)]-[node (6 bytes)] // 7 bytes (9-15), bits 72 to 127
        $bin = UidHelper::randomBytesAsBinary(16);

        // Set the four most significant bits (bits 12 through 15) of the time-high-and-version field to the
        // 4-bit version number (0100).
        $bin = substr_replace($bin, '0100', 48, 4);

        // Set the two most significant bits (bits 6 and 7) of the clock-seq-and-reserved field to the
        // 2-bit variant/type identifier (10).
        $bin = substr_replace($bin, '10', 64, 2);

        // Convert to hex and add a separator before the following bytes: 4, 6, 8, 9, 10
        $hex = str_pad(UidHelper::bin2hex($bin), 32, '0', STR_PAD_LEFT);
        $identifier = sprintf(
            '%1$s-%2$s-%3$s-%4$s-%5$s',
            substr($hex, 0, 8),
            substr($hex, 8, 4),
            substr($hex, 12, 4),
            substr($hex, 16, 4),
            substr($hex, 20),
        );

        return UidHelper::acceptIdentifier($identifier, $minLength, $maxLength);
    }
}
