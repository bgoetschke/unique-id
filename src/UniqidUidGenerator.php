<?php

declare(strict_types=1);

namespace BjoernGoetschke\UniqueID;

/**
 * Generates unique identifiers based on the uniqid() function.
 *
 * @api usage
 * @since 3.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
final class UniqidUidGenerator implements UidGeneratorInterface
{
    public function generate(int $minLength = 0, int $maxLength = 0): string
    {
        $moreEntropy = false;
        if ($minLength > 13) {
            $moreEntropy = true;
        }

        $identifier = uniqid('', $moreEntropy);

        return UidHelper::acceptIdentifier($identifier, $minLength, $maxLength);
    }
}
