ARG PHP_VERSION
ARG COMPOSER_VERSION

FROM composer:${COMPOSER_VERSION:?} AS composer

FROM php:${PHP_VERSION:?}

ARG XDEBUG_VERSION

#
# Common environment setup
#
RUN export PHP_VERSION_ID=$(php -r 'echo PHP_VERSION_ID;') && \
    apt-get update && \
    apt-get -y upgrade && \
    apt-get -y install \
        libicu-dev \
        libzip-dev \
        libxml2-dev \
        libxslt-dev \
        libpng-dev \
        libc-client-dev \
        libkrb5-dev \
        libldap2-dev \
        libgmp-dev \
        $PHPIZE_DEPS && \
    apt-get clean && \
    find /var/lib/apt/lists/ -type f -delete && \
    docker-php-ext-install -j$(nproc) \
        mysqli \
        pdo_mysql \
        intl \
        zip \
        soap \
        xsl \
        gd \
        ldap \
        bcmath \
        gmp \
        pcntl && \
    if test $PHP_VERSION_ID -lt 80400; then \
        echo "PHP setup < 8.4 ($PHP_VERSION_ID)" && \
        docker-php-ext-configure \
            imap --with-kerberos --with-imap-ssl && \
        docker-php-ext-install -j$(nproc) \
            imap ; \
    else \
        echo "PHP setup >= 8.4 ($PHP_VERSION_ID)" && \
        pecl install imap ; \
    fi && \
    mkdir /project

RUN echo 'display_errors = Off' >> /usr/local/etc/php/conf.d/local.ini && \
    echo 'log_errors = On' >> /usr/local/etc/php/conf.d/local.ini && \
    echo 'default_mimetype =' >> /usr/local/etc/php/conf.d/local.ini && \
    echo 'default_charset = UTF-8' >> /usr/local/etc/php/conf.d/local.ini && \
    echo 'input_encoding = UTF-8' >> /usr/local/etc/php/conf.d/local.ini && \
    echo 'output_encoding = UTF-8' >> /usr/local/etc/php/conf.d/local.ini && \
    echo 'internal_encoding = UTF-8' >> /usr/local/etc/php/conf.d/local.ini

ENV HOME=/
WORKDIR /project

#
# Development setup
#
ENV COMPOSER_CACHE_DIR=/dev/shm/composer-cache/
COPY --from=composer /usr/bin/composer /usr/local/bin/

RUN cp /usr/local/etc/php/php.ini-development /usr/local/etc/php/php.ini && \
    pecl install \
        xdebug-${XDEBUG_VERSION:?} && \
    ln -s /project/xdebug.ini /usr/local/etc/php/conf.d/xdebug.ini

#
# Additional configuration for the tools container
#
RUN echo 'memory_limit = -1' >> /usr/local/etc/php/conf.d/local.ini && \
    apt-get update && \
    apt-get -y upgrade && \
    apt-get -y install \
        unzip \
        git && \
   apt-get clean && \
   find /var/lib/apt/lists/ -type f -delete
