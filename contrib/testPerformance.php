<?php

declare(strict_types=1);

// ========== SETUP ==========

/// set default php settings
http_response_code(500);
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', (PHP_SAPI !== 'cli' && Phar::running() === '') ? '1' : '0');
ini_set('log_errors', '1');
ini_set('html_errors', '0');
ini_set('default_mimetype', '');
ini_set('default_charset', 'UTF-8');
mb_detect_order(['UTF-8', 'ISO-8859-15', 'ISO-8859-1', 'CP1252', 'CP1251']);
mb_internal_encoding('UTF-8');
mb_regex_encoding('UTF-8');

// initialize autoloader
require dirname(__DIR__) . '/vendor/autoload.php';

// ========== RUN PERFORMANCE TEST ==========

// parse arguments
/** @var mixed[] $opts */
$opts = getopt('t::');
if (!array_key_exists('t', $opts)) {
    $opts['t'] = 10;
}
$opts['t'] = (int)$opts['t'];
if ($opts['t'] < 1) {
    $msg = 'Time limit must be at least one (-t)';
    trigger_error($msg, E_USER_ERROR);
}

// run tests
$testGenerator = function ($t, $g): int {
    $s = hrtime(true) + ($t * 1000000000);
    $i = 0;
    while (hrtime(true) < $s) {
        $g->generate(20, 40);
        $i++;
    }
    return $i;
};
$r = [
    'Datetime' => $testGenerator($opts['t'], new BjoernGoetschke\UniqueID\DatetimeUidGenerator()),
    'Timestamp' => $testGenerator($opts['t'], new BjoernGoetschke\UniqueID\TimestampUidGenerator()),
    'Uniqid' => $testGenerator($opts['t'], new BjoernGoetschke\UniqueID\UniqidUidGenerator()),
    'Uuid4' => $testGenerator($opts['t'], new BjoernGoetschke\UniqueID\Uuid4UidGenerator()),
    'Alphabet' => $testGenerator($opts['t'], new BjoernGoetschke\UniqueID\AlphabetUidGenerator()),
    'Ulid' => $testGenerator($opts['t'], new BjoernGoetschke\UniqueID\UlidUidGenerator()),
    'Cuid' => $testGenerator($opts['t'], new BjoernGoetschke\UniqueID\CuidUidGenerator()),
];

// be sure we have an actual result
$m = max($r);
if ($m < 1) {
    echo 'max = 0';
    die(1);
}

// display results
foreach ($r as $g => $c) {
    $p = bcmul((string)bcdiv((string)$c, (string)$m, 20), '100', 2);
    $s = bcdiv((string)$c, (string)$opts['t'], 2);
    echo $g . ': ' . $c . ' total, ' . $s . ' per second, ' . $p . '% of best' . PHP_EOL;
}
