<?php

declare(strict_types=1);

// ========== SETUP ==========

// set default php settings
http_response_code(500);
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', (PHP_SAPI !== 'cli' && Phar::running() === '') ? '1' : '0');
ini_set('log_errors', '1');
ini_set('html_errors', '0');
ini_set('default_mimetype', '');
ini_set('default_charset', 'UTF-8');
mb_detect_order(['UTF-8', 'ISO-8859-15', 'ISO-8859-1', 'CP1252', 'CP1251']);
mb_internal_encoding('UTF-8');
mb_regex_encoding('UTF-8');

// initialize autoloader
require dirname(__DIR__) . '/vendor/autoload.php';

// ========== RUN PARALLEL GENERATION TEST ==========

// parse arguments
/** @var mixed[] $opts */
$opts = getopt('p:m:g:t::sc');
if (!array_key_exists('p', $opts)) {
    $opts['p'] = 0;
}
$opts['p'] = (int)$opts['p'];
if ($opts['p'] < 1) {
    $msg = 'Number of processes must be at least one (-p)';
    trigger_error($msg, E_USER_ERROR);
}
if (!array_key_exists('t', $opts)) {
    $opts['t'] = 10;
}
$opts['t'] = (int)$opts['t'];
if ($opts['t'] < 1) {
    $msg = 'Time limit must be at least one (-t)';
    trigger_error($msg, E_USER_ERROR);
}
if (!array_key_exists('m', $opts)) {
    $opts['m'] = 10;
}
$opts['m'] = (int)$opts['m'];
if ($opts['m'] < 0) {
    $msg = 'Minimum lenth of uids must be at least zero (-m)';
    trigger_error($msg, E_USER_ERROR);
}
if (!array_key_exists('g', $opts)) {
    $opts['g'] = '';
}
if (!array_key_exists('s', $opts)) {
    $opts['s'] = false;
} else {
    $opts['s'] = true;
}
if (!array_key_exists('c', $opts)) {
    $opts['c'] = false;
} else {
    $opts['c'] = true;
}
$generators = [
    'Datetime' => new BjoernGoetschke\UniqueID\DatetimeUidGenerator(),
    'Timestamp' => new BjoernGoetschke\UniqueID\TimestampUidGenerator(),
    'Uniqid' => new BjoernGoetschke\UniqueID\UniqidUidGenerator(),
    'Uuid4' => new BjoernGoetschke\UniqueID\Uuid4UidGenerator(),
    'Alphabet' => new BjoernGoetschke\UniqueID\AlphabetUidGenerator(),
    'Ulid' => new BjoernGoetschke\UniqueID\UlidUidGenerator(),
    'Cuid' => new BjoernGoetschke\UniqueID\CuidUidGenerator(),
];
if (!array_key_exists($opts['g'], $generators)) {
    $msg = 'Invalid generator specified (-g), valid values: ' . implode(', ', array_keys($generators));
    trigger_error($msg, E_USER_ERROR);
}
$generator = $generators[$opts['g']];

// create childs/generate uids
echo 'Using generator (-g): ' . get_class($generator) . PHP_EOL;
echo 'Minimum uid length (-m): ' . $opts['m'] . PHP_EOL;
if ($opts['s']) {
    echo 'Adding pid suffix to generated uids (-s)' . PHP_EOL;
} else {
    echo 'Not adding pid suffix to generated uids (-s not set)' . PHP_EOL;
}
if ($opts['c']) {
    echo 'Per process collision checking enabled (-c)' . PHP_EOL;
} else {
    echo 'Per process collision checking disabled (-c not set)' . PHP_EOL;
}
echo 'Creating ' . $opts['p'] . ' child(s) ...' . PHP_EOL;
$childs = [];
while (count($childs) < $opts['p']) {
    $pid = pcntl_fork();
    if ($pid < 0) {
        assert(count($childs) > 0);
        foreach ($childs as $childPid) {
            posix_kill($childPid, SIGKILL);
        }
        $msg = 'Failed to fork child :(';
        trigger_error($msg, E_USER_ERROR);
    } elseif ($pid > 0) {
        $childs[] = $pid;
    } else {
        $file = __DIR__ . '/testParallel.' . getmypid() . '.out';
        $fh = fopen($file, 'w+');
        if ($fh === false) {
            $msg = 'Failed to open file: ' . $file;
            trigger_error($msg, E_USER_ERROR);
        }
        if ($opts['s']) {
            $generator = new BjoernGoetschke\UniqueID\AddAffixUidGeneratorDecorator($generator, '', '.' . getmypid());
        }
        if ($opts['c']) {
            $generator = new BjoernGoetschke\UniqueID\CheckCollisionUidGeneratorDecorator(
                $generator,
                function (array $newUids): ?string {
                    static $generatedUids = [];
                    foreach ($newUids as $newUid) {
                        if (!in_array($newUid, $generatedUids, true)) {
                            $generatedUids[] = $newUid;
                            return $newUid;
                        }
                    }
                    return null;
                },
            );
        }
        while (true) {
            fwrite($fh, $generator->generate($opts['m']) . PHP_EOL);
            fflush($fh);
        }
    }
}

// kill all childs after 10 seconds
echo 'Waiting ' . $opts['t'] . ' second(s) for all childs to generate some uids ...' . PHP_EOL;
sleep($opts['t']);
echo 'Killing all childs ...' . PHP_EOL;
foreach ($childs as $childPid) {
    posix_kill($childPid, SIGKILL);
}

// read all generated uids and unlink tmp files
echo 'Processing generated uids ...' . PHP_EOL;
$uids = [];
$examples = [];
foreach ($childs as $childPid) {
    $storeExample = true;
    $file = __DIR__ . '/testParallel.' . $childPid . '.out';
    if (!is_file($file)) {
        $msg = 'Missing file: ' . $file;
        trigger_error($msg, E_USER_NOTICE);
        continue;
    }
    $newUids = file($file);
    unlink($file);
    if (!is_array($newUids)) {
        continue;
    }
    foreach ($newUids as $uid) {
        $uid = trim($uid);
        if (strlen($uid) < 1) {
            continue;
        }
        if (!isset($uids[$uid])) {
            $uids[$uid] = 0;
        }
        $uids[$uid]++;
        if ($storeExample) {
            $examples[] = $uid;
            $storeExample = false;
        }
    }
}

// show summary
$total = array_sum($uids);
$unique = count($uids);
$uniquePercent = 0;
if ($total > 0) {
    $uniquePercent = ($unique / $total * 100);
}
$duplicates = ($total - $unique);
$maxPerUid = 0;
if ($unique > 0) {
    $maxPerUid = max($uids);
}
echo 'Total: ' . $total . PHP_EOL;
echo 'Unique: ' . $unique . ' (' . number_format($uniquePercent, 2, '.', '') . '%)' . PHP_EOL;
echo 'Duplicates: ' . $duplicates . PHP_EOL;
echo 'MaxPerUid: ' . $maxPerUid . PHP_EOL;
echo 'Examples: ' . implode(', ', $examples) . PHP_EOL;
